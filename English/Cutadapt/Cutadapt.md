## Cutadapt

[Cutadapt](https://cutadapt.readthedocs.io/en/stable/) is a frequently used tool in the field of high-throughput sequencing. It effectively removes adapters, poly-A tails, and low-quality sequences that can interfere with data analysis.

It was developed by the group of [Dr Sven Rahmann](https://www.rahmannlab.de/people/rahmann) at [Dortmund University](https://www.tu-dortmund.de/)

You can submit issues related to Cutadapt [in the gitlab](https://github.com/marcelm/cutadapt/issues)

In the RNAseq pipeline, Cutadatp is used via [Trim Galore](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/) which is a combination of Cutadapt and FastQC tools.

For RNA-seq data, Cutadapt is used in part 1, raw data processing. The data are Illumina-sequenced RNA reads.

<img src="../../img/PipelineA_RNAseq_TrimGalore.png">

**Figure 1 : simplified RNAseq pipeline.**