## Cutadapt

### Filtred Reads

<img src="../../img/cutadapt_filtered_reads_plot-1.png" title="Filtred reads" width="650" height="400"> 

**Figure 1 : Barplot of the number of reads passing the cutadapt filter for each sample.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The graph above shows the number of reads that have passed the filter on the x-axis, and the samples on the y-axis. These are the reads from which the adapters have been removed. If a black metric such as **Pairs that were too short** appears, this means that the reads for this metric did not pass the filter because they were too small. It's up to the user to decide whether or not it's normal to have unfiltered data. See figure 2 for an example.

<img src="../../img/cutadapt_filtered_reads_plot.png" width="650" height="400"> 

**Figure 2 : barplot of the number of reads passing the cutadapt filter for each sample with too small reads.**

*source : [nf-core RNAseq MultiQC](https://training.galaxyproject.org/training-material/topics/transcriptomics/tutorials/ref-based/tutorial.html)*