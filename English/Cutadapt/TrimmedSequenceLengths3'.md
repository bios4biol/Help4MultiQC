## Cutadapt

### Trimmed Sequence Lenght 3'

In the 3’ section of the reads, the sequencing process, such as the one used by Illumina, can leave adapters in the reads. These adapters are then removed by Cutadapt.

### Counts :

<img src="../../img/cutadapt_trimmed_sequences_plot_3-1.png" title="Trimmed Sequence Lenght 3' Counts" width="650" height="400">

**Figure 1 : count of reads trimmed by Cutadapt in terms of length for each sample.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

In Figure 1, each sample curve has a different color, but they overlap. This is exactly what we expect from Cutadapt. A large number of short 3' ends (up to 5 bases) are cleaned. To conclude, this depends on the position of the adapters. For example, in the case of sRNAs, the adapter may be located in the middle of the read. In this case, the cut length is greater than 10 bases.

### Obs/Exp :

<img src="../../img/cutadapt_trimmed_sequences_plot_3-1(1).png" title="Trimmed Sequence Lenght 3' Obs/Exp" width="650" height="400">

**Figure  2 : observed lengths/expected lengths of adapters cut by cutadapt for each sample reading represented by different colors.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The graph in figure 2 is the ratio of observed to expected data as the ordinate. The expected data are calculated by Cutadapt. In this graph, each sample is represented by a color, and disparities can be seen in the center of the figure in the form of different peaks. These peaks illustrate the differences between expected and observed values. Although these differences may seem worrying, it's important to note that the unexpected values around 13 to 63 bases are very small, as are the expected values (we're talking about less than 1,000 when compared with the 30 million for the firsts lengths in figure 1). The values should therefore be compared with Figure 1 to see if there are peaks with a large number of bases.