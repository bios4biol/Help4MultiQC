## DESeq2

[DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html) is available on [Bioconductor](https://bioconductor.org/) and is maintained by Michael Love. It is an R library that estimates the mean-dependent variance in count data from high-throughput sequencing.

It allows for performing PCA among sample data to identify the closest samples.

Here the [reference manual](https://bioconductor.org/packages/3.19/bioc/manuals/DESeq2/man/DESeq2.pdf), and an exemple of [RNAseq](https://bioconductor.org/packages/3.19/bioc/vignettes/DESeq2/inst/doc/DESeq2.html) data treatment.

For RNA-seq data, DESeq2 is used in part 5, statistics. The data used for DESeq2 are the aligned transcripts and quantified values of differential genome expression for the different samples.

<img src="../../img/PipelineA_RNAseq_DESeq2.png">

**Figure1 : simplified RNAseq pipeline.**