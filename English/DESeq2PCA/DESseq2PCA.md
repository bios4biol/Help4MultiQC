## DESeq2

### ACP : Principal Component Analysis

<img src="../../img/salmon_deseq2_pca-plot.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1: PCA generated from the quantification matrix calculated by RSEM.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

PCAs are a good tool for validating biological samples, with verification of good batch separation. In the example above, each point represents a sample. The points are grouped in pairs because each sample is represented close to its replicate. This is a good indicator that the experimental steps have gone smoothly.

<img src="../../img/DESeq2PLOT.png"> 

**Fidure 2: différences dans les composantes principales entres les préfixes des échantillons (origine ou réplicat).**

*source : [nf-core RNAseq Output](https://nf-co.re/rnaseq/3.14.0/docs/output/#deseq2)*
