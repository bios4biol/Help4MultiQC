## DESseq2

### Heatmap

<img src="../../img/star_rsem-plot.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1: heatmap of sample similarities based on Euclidean distance deviations. The counting matrix used is the result of STAR mapping followed by RSEM quantification.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The heat map represents the comparative values obtained after calculating the Euclidean distances in the previous PCA. Here, the bluer the squares, the closer the samples are; the redder they are, the further apart they are. This graph can be used to analyze the differences between each sample.