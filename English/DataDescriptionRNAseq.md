# Data description

In this guide, the graphics are sourced from pipeline presentation sites.

## RNAseq data

The RNAseq data come from the pipeline [Nextflow nf-core RNAseq](https://nf-co.re/rnaseq/3.14.0/):

<img src="../img/nf-core-rnaseq_metro_map_grey.png" width="700" height="350">

**Figure 1 : metro map of all RNAseq pipeline paths..**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The RNAseq pipeline is a comprehensive process that can follow different paths depending on the biological questions asked or on the initial data. The guide describes most of the available graphs, but the conclusions drawn from these graphs can vary depending on the pipeline or the biological question asked.

*source : [RNAseq data ](https://github.com/nf-core/test-datasets/blob/rnaseq/README.md#full-test-dataset-origin)*

The data used are paired-end human genome data, obtained through sequencing with Illumina HiSeq 2000 (GM12878 and K562) or Illumina Genome Analyzer IIx (for MCF-7 and H1-hESC). These data were employed in a global [human sequencing project](https://pubmed.ncbi.nlm.nih.gov/22955616/) and in a project aimed at measuring the quality of quantitative RNAseq pipelines. Consequently, a large number of duplicated data is expected.

The sequencing of the four samples was replicated three times. All samples come from *Homo sapiens*.

