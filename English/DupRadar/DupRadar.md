## DupRadar

[Dupradar](https://bioconductor.org/packages/release/bioc/html/dupRadar.html) is a sophisticated tool that allows for the examination of the duplication rate present in samples.

Made by [Sergi Sayols](https://www.uu.se/en/contact-and-organisation/staff?query=N19-1558), here is the [link](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1276-2) to the publication article.

For RNA-seq data, DupRadar is used in "part 5 statistics". The data used by DupRadar are the aligned reads to estimate the level of duplications for each sampler.

<img src="../../img/PipelineA_RNAseq_dupRadar.png">

**Figure 1 : simplified RNAseq pipeline.**