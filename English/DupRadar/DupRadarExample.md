## DupRadar

### graphical results

<img src="../../img/dupradar_multiplot.png" title="GLM Dupradar" width="650" height="400">

**Figure 1 :a case with no duplication problems to the left and a case with duplication problems to the right.**

*source : [nf-core RNAseq description](https://pilm-bioinformatics.github.io/pipelines-nf-rnaseq/output.html)*

In figure 1, the graphs have the rate of duplicated reads on the ordinate and the expression level expressed in reads/kbp (1000 base pairs) on the abscissa.
These graphs of regression lines perfectly illustrate the 2 possible cases. In the case of conventional sequencing, the greater the sequencing depth, the higher the number of duplicates. Dupradar allows you to visualize the levels of duplicates obtained as a function of the expression found for each sample. In an ideal case, the duplicate rate increases suddenly, but remains low (less than 10%) at low expression levels. On the other hand, in a case where there is a duplication problem, the points tend to represent a uniform egg shape, resulting in a continuously increasing regression curve. In the second case, it is not the sequencing depth but the level of duplication of the sequence that is responsible for the number of duplicates.

<img src="../../img/dupradar-plot-1.png" title="Alignement stats" width="650" height="400">

**Figure 2 : generalized linear Dupradar models modeling the expression of duplicate data for each sample.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The values shown in Figure 2 are taken from RNAseq data. A very high level of duplication is observed, particularly for the four samples at the top of the graph. This may be typical for RNAseq data, given that some genes, such as mitochondrial genes, may be much more expressed than others. There is also ribosomal RNA, which is a highly duplicated gene in the genome. As a result, this sequence can be found many times in the transcriptome, giving rise to multimapping and duplications. It is therefore very difficult to measure the differential expression of ribosomal RNA.