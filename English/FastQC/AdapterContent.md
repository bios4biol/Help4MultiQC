## FastQC

### Adapter Content

If your sequence contain very few adapter you will see the following message : 

<img src="../../img/CaptureAdapterAllGood.PNG" title="less than 1 percent of adapter in reads"/>

If the whole curve is in the green zone, the adapter content is low.
If you click on the curve, you will find information about the amount of adapter at the selected position and the type of adapter detected.

<img src="../../img/fastqc_adapter_content.png" title="good adapter content"/>

**Figure 1 : graph showing the percentages of adapters found at different positions in the reads.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

If you have some curves in the red, they contain a very high amount of adapters. The percentage of sequencing containing adapter is represented on the y-axis and on the x-axis it is the position on the reading. In the example below the black curve means that from the 10th base of the read about 65% of the reads are adaptor. 

<img src="../../img/fastqc_adapter_content_plot_bad.png" title="bad adapter content"/>

**Figure 2 : graph showing the percentages of adapters found at different positions in the reads (unsatisfactory results).**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

Be careful to use the scrollers to go to the end of the reads on the right when they are relatively long.


<img src="../../img/fastqc_adapter_contentex.png"/>

**Figure 3 : schematic of expected results.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*

### Warning

This module will issue a warning if any sequence is present in more than 5% of all reads.

### Failure

This module will issue a warning if any sequence is present in more than 10% of all reads.

### Common reasons for warnings

Any library where a reasonable proportion of the insert sizes are shorter than the read length will trigger this module. This doesn't indicate a problem as such - just that the sequences will need to be adapter trimmed before proceeding with any downstream analysis.
