# FastQC

## Per base N content

Some sequencer substitute a N when it's unable to make a base call with sufficient confidence. This graph shows the percentage of N base calls at each position.

<img src="../../img/NContent.PNG" title="N content plot"/>

**Figure 1 : graph representing the percentage of N in the reads.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

A low proportion of N at the end of the sequence is not a cause for concern. There is often more N on read 2 than on read 1, this is also normal.
If the proportion becomes too high, this indicates degradation of the sequenced material for example.

Novaseq sequencer do not use N anymore. On the other hand, MiSeq and HiSeq from Illumina can call Ns.

<img src="../../img/fastqc_per_base_n_content.png"/>

**Figure 2 : schematic of expected results.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*