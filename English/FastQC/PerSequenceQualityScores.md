# FastQC

## Per sequence quality scores

![Per Sequence Quality Score](../../img/PerSequenceQualityScore.png "Per Sequence Quality Score") 

**Figure 1 : graph representing the quality score by the number of reads for each sample.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

This graph allows you to evaluate the average quality of your sequences. If the average quality of the run impacts a small percentage of sequences, the distribution will be correct (acceptable error rate). If not, it means that there is a specific problem that you will need to determine.

### Warning

A warning is issued if the average most frequently observed quality is less than 27 - equivalent to an error rate greater than 0.2%.

### Failure

An error is triggered if the average most frequently observed quality is less than 20 - equivalent to an error rate greater than 1%.

### Common reasons for warnings

![Per Sequence Quality Score bad data](../../img/fastqc_per_sequence_quality_score.png "Per Sequence Quality Score") 

**Figure 2 : Diagram of the expected results**