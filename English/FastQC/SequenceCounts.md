# FastQC

## Sequence counts

This module resumes the sequence counts for each sample. 

<img src="../../img/fastqc_sequence_counts_plot.png" title="sequence_counts_plot" height="400"/>

**Figure 1 : bar chart representing the number or percentage of unique or duplicated reads.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

The proportion of unique (light blue) and duplicated (dark gray) reads are illsutrated as a bar chart. 

Duplicate read counts are an estimate only and are defined in the section [SequenceDuplicationLevels](https://bios4biol.pages.mia.inra.fr/Help4MultiQC/FastQC/SequenceDuplicationLevels.html).

<img src="../../img/fastqc_sequence_counts.png"/>

**Figure 2 : schematic of expected results.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*