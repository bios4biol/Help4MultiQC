# FastQC

## Sequence Quality Histograms

This module illustrates the mean quality value across each base position in the read.
<img src="../../img/fastqc_per_base_sequence_quality_plot.png" title="sequence Quality Histogram" height="400"/>

**Figure 1 : graph representing the average quality of the reads.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

The y-axis on the graph shows the quality scores. The higher the score the better the base call. The background of the graph divides the y axis into very good quality calls (green), calls of reasonable quality (orange), and calls of poor quality (red). The quality of calls on most platforms will degrade as the run progresses, so it is common to see base calls falling into the orange area towards the end of a read. 


### Warning

A warning will be issued if the lower quartile for any base is less than 10, or if the median for any base is less than 25.

### Failure

This module will raise a failure if the lower quartile for any base is less than 5 or if the median for any base is less than 20.

### Common reasons for warnings

The most common reason for warnings and failures in this module is a general degradation of quality over the duration of long runs. In general sequencing chemistry degrades with increasing read length and for long runs you may find that the general quality of the run falls to a level where a warning or error is triggered.

If the quality of the library falls to a low level then the most common remedy is to perform quality trimming where reads are truncated based on their average quality. For most libraries where this type of degradation has occurred you will often be simultaneously running into the issue of adapter read-through so a combined adapter and quality trimming step is often employed.

Another possibility is that a warn / error is triggered because of a short loss of quality earlier in the run, which then recovers to produce later good quality sequence. This can happen if there is a transient problem with the run (bubbles passing through a flowcell for example). You can normally see this type of error by looking at the per-tile quality plot (if available for your platform). In these cases trimming is not advisable as it will remove later good sequence, but you might want to consider masking bases during subsequent mapping or assembly.

If your library has reads of varying length then you can find a warning or error is triggered from this module because of very low coverage for a given base range. Before committing to any action, check how many sequences were responsible for triggering an error by looking at the sequence length distribution module results. 

<img src="../../img/fastqc_sequence_quality_histogram.png"/>

**Figure 2 : schematic of expected results.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*