# FastQC

## Status Check

This module resumes the status results for all the fastQC analysis as a heatmap illustrating **Passing** (green), **Warnings** (orange) and **Fails** (Red).

The following image illustrates classical status pattern for a RNAseq analysis.

<img src="../../img/fastqc-status-check-heatmap.png" title="fastqc-status-check-heatmap" height="400"/>

**Figure 1 : status for each FastQC test performed for each sample.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*
