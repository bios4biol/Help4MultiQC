# FastQC

## Troubleshooting

### Sequencing quality

The sequencing quality is not necessarily correct. 
That's the reason why some alignment software recalculates the quality: GATK.


### Technical biais

#### **Bias due to hexamer random priming**

This bias impacts the composition of the beginning of the sequences.

![image](../../img/hexamer.png "Hexamer random priming") 

**Figure 1 : graphical representation of the percentages of A, T, C, or G in the reads for a sample.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*


#### **Bias due to a contamination**

This graph is an example of a GC percent bias that could indicate sample contamination with another organisme or a fragment.

![image](../../img/contamination.png "Bias due to contamination") 

**Figure 2 : graph representing the percentage of GC in the reads.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*