# FastQC software

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) is a software developped by [Babraham Bioinformatics](https://www.bioinformatics.babraham.ac.uk/) group from [Babraham Institute](https://www.babraham.ac.uk/).


FastQC aims to provide a simple way to do some quality control checks on raw sequence data coming from high throughput sequencing pipelines. It provides a modular set of analyses which you can use to give a quick impression of whether your data has any problems of which you should be aware before doing any further analysis. 

On the [FastQC project page](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) you can find a [Documentation resource](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/) and some example reports.

Some posts dealing with FastQC fails cases are accessible on [https://sequencing.qcfail.com/software/fastqc/](https://sequencing.qcfail.com/software/fastqc/).


For RNA-seq data, FastQC is used twice in Part 1, data preparation. It uses unaligned reads in fastq format to perform pre- and post-trimming checks for adapters and poor-quality sequences.

<img src="../../img/PipelineA_RNAseq_FastQC.png">

**Figure 1 : simplified RNAseq pipeline.**