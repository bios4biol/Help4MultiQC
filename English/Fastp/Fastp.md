# FastP

[Fastp](https://academic.oup.com/bioinformatics/article/34/17/i884/5093234) is a sequence quality processing tool. It is responsible for cutting adapters, deleting sequences that are too small or too long, or that have poor quality.

It was created by [Shifu Chen](https://scholar.google.com/citations?user=tW47uPIAAAAJ), [Zhou Yanqing](https://www.researchgate.net/profile/Zhou_Yanqing2), [Yaru Chen](https://scholar.google.com/citations?user=nntUUqcAAAAJ) and [Jia Gu](https://jia-gu.github.io/).

FastP is used for data pre-processing, so it uses sequences in FastQ format.