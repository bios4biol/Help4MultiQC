## GC Content

<img src="../../img/fastp-seq-content-gc-plot.png"> 

**Figure 1 : average GC percentage of readings for each sample before filtering.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

In Figure 1, the x-axis is composed of the reads positions and the y-axis of the GC rate. 
GC rate is a classic metric for measuring the quality of reads. For genes, we expect a ratio of 50% constant on the reads. 
Here it's clearly what we're seeing, so all's well.

<img src="../../img/fastp-seq-content-gc-plot(1).png"> 

**Figure 2 : average GC percentage of readings for each sample after filtering.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

In Figure 2, after filtering, there is no difference with Figure 1, so most of the information has been retained and we can conclude that the sequencing was of very good quality.