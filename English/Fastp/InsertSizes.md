## Insert Size

The insert size is a sequence including 2 paired reads and the inner distance.

<img src="../../img/fgene-05-00005-g001.jpg"  width="650" height="400"> 

**Figure 1 :  representation of an insert size during Illumina sequencing.**

*Source : [Filter BAM/SAM files by insert size](https://accio.github.io/bioinformatics/2020/03/10/filter-bam-by-insert-size.html)*




<img src="../../img/fastp-insert-size-plot.png">

**Figure 2 : percentage of insert size lengths for each sample.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

Figure 2 shows the distributions of the various insert sizes, with an increasing statistic that peaks at around 150 bases pairs before falling back down. Given that the reads are around 120 bases pairs, the innner distance is mostly around -90. 