## N Content


<img src="../../img/fastp-seq-content-n-plot.png.png">

**Figure 1 : rate of average N readings for each sample before filtering**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

In Figure 1, the position of the reads is shown on the x-axis and the average N rate of the reads on the y-axis. The 2 samples are shown in the graph, but they overlap. 
Figure 1 shows very low values. This is to be expected from high quality samples.

<img src="../../img/fastp-seq-content-n-plot.png(1).png"> 

**Figure 2 :  rate of average N readings for each sample after filtering**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

We can clearly see that there is no great evolution after filtering, which means that the sequencing went very well and the data is of very high quality.