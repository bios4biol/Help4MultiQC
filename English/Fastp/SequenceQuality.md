## Qualité des lectures

<img src="../../img/fastp-seq-quality-plot.png"> 

**Figure 1 : average quality of readings for each sample before filtering.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

In Figure 1, the position of the reads is shown on the x-axis and the average quality of the readings on the y-axis. The 2 samples are shown in the graph, but they overlap. 

We can see 2 things: that for the first and last nucleotides, quality is lower than for the rest of the bases, and that quality decreases as we move towards the 5' end. This is a common occurrence, so don't be too careful. Qualities below 30 can be problematic and will surely be trimmed.

<img src="../../img/fastp-seq-quality-plot(2).png"> 

**Figure 2 : average quality of readings for each sample after filtering.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

We can clearly see that there is no great evolution after filtering, which means that the sequencing went well and the data quality is high.