## Feature Counts

[Feature counts](https://subread.sourceforge.net/featureCounts.html) is a genomic data summarization tool. It allows counting genome features such as genes, exons, introns, and promoters.

It takes as input SAM/BAM files of the tested sequences and a GTF annotation file of the reference genome. It is part of the [Subread](https://subread.sourceforge.net/) package.

For RNA-seq data, FeatureCounts is used in part 4, statistics. It uses the alignment obtained on the reference genome to estimate which biotypes are expressed.

<img src="../../img/PipelineA_RNAseq_FeatureCounts.png">

**Figure1 : simplified RNAseq pipeline.**