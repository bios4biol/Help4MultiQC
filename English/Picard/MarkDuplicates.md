## Picard

### Mark Duplicates

MarkDuplicates uses the information contained in BAM/SAM files to present duplication levels in samples.

<img src="../../img/mqc_picard_deduplication_1_pc.png" title="Mark duplicates" width="650" height="400"> 

**Figure 1 : horizontal bar plots of the differents type of paired reads (as percentages)**

*source : [nf-core SAREK MultiQC](https://nf-co.re/sarek/3.4.2/results/sarek/results-b5b766d3b4ac89864f2fa07441cdc8844e70a79e/test_aws/multiqc/?file=multiqc_report.html)*

**Unique pairs** refers to reads paired with only one other read. **Duplicate Pairs** Optical refers to duplicates associated with incorrect cluster identification by Illumina sequencing. **Duplicate Pairs** refers to paired duplicates that are not related to Illumina's optical problem. This may be due to overexpression of a gene, a PCR problem, clustering (a situation where a cluster occupies two wells during its generation) or by Sister, where duplicates appear following the creation of complementary strands of sequences from the original cluster. **Duplicate Unpaired** refers to duplicate readings that are neither sequenced nor mapped. **Unmapped** corresponds to non-aligned reads and **Unique Unpaired** refers to reads without pairing or duplicates

<img src="../../img/Duplicates.illumina.png" title="duplicates illumina" width="650" height="400"> 

**Figure 2 : duplicate causes in Illumina sequencing.**

*source : [Illumina description](http://core-genomics.blogspot.com/2016/01/almost-everything-you-wanted-to-know.html)* 

<img src="../../img/picard_deduplication-1.png" title="Mark duplicates"  width="650" height="400"> 

**Figure 3 : horizontal barplot of the differents type of paired reads (as percentages) for multiple samples.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

In RNAseq data, it is common to find such rates of duplications. However, it is important to note that these rates are significant. This situation could be due to an overexpression of certain genes or problems related to Illumina sequencing.

### For data other than RNAseq :

<img src="../../img/picard_deduplication_stats.png" > 

**Figure 4 : conclusion to do on the differents graphics results.**

*Source : [nf-core eager](https://nf-co.re/eager/2.5.2/docs/output/)*

Fewer duplicates are expected with DNAseq.