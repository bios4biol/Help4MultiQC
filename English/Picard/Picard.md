## Picard

[Picard](https://broadinstitute.github.io/picard/) is a set of tools that allows for the efficient manipulation of high-throughput sequencing data.

It is possible to report issues related to Picard on their [GitHub](https://github.com/broadinstitute/picard/issues).

For RNA-seq data, Picard is used in part 4, processing aligned data. It uses the aligned reads to quantify duplicated one's.

<img src="../../img/PipelineA_RNAseq_Picard.png">

**Figure 1 : simplified RNAseq pipeline.**