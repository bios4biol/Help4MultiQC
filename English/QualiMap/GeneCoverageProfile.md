## QualiMap

## Gene Coverage Profile

The Quali Map tool uses annotations from GTF files from reference genome to analyze the coverage of reads for each sample.

<img src="../../img/qualimap_gene_coverage_profile-1.png" title="Gene Coverage Profile"> 

**Figure 1 : Genome coverage for each sample with count values**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

In the first figure, the ordinate represents the cumulative depth of the mapped reads and the abscissa represents the position in the transcript divided by 100. Each sample is represented by a different color, allowing comparison of sample sizes, but not yet their overlap.

The graph shows lower values at the 5’ and 3’ ends. This is normal, as fewer reads pass quality control at these ends due to their smaller size or a greater number of errors during sequencing. The difference should be the same at both ends, as the 5' and 3' ends are not calculated separately. In the example above, there is a difference in the last percentages of gene coverage, which is much lower than the first percentages.