## QualiMap

### Genomic Origin of reads

QualiMap generates various graphs to assist in the quality control of alignment sequencing data. It derives this information from GTF annotation files of the reference genome.

<img src="../../img/qualimap_genomic_origin-1.png" title="Gene Origin of reads"> 

**Figure 1 : genomic origin of reads for each sample.**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

This graph shows the genomic origin of the data. For RNAseq data, we expect a high proportion of reads from exons due to the transcriptomic nature of the RNA sequenced. The intragenic parts come from introns, and the intergenic regions include all the rest.