## QualiMap

[Qualimap](qualimap.conesalab.org) is a sequencing evaluation tool. It can process RNAseq, ChIP-seq, whole genome sequencing, and whole exome sequencing data.

For any questions, it is possible to join the [Qualimap Google group](https://groups.google.com/g/qualimap).

It was created by [Konstantin Okonechnikov](https://www.dkfz.de/en/paediatrische-neuroonkologie/staff/Konstantin_Okonechnikov.html), [Sonia Tarazona](https://www.researchgate.net/profile/Sonia-Tarazona) and [Ana Conesa](https://scholar.google.com/citations?user=KMTiIH4AAAAJ) in connection with [the Max Planck Institute for Infectious Biology](https://www.mpiib-berlin.mpg.de/) and the [CIPF](https://bioinfo.cipf.es/cbl/)

For RNA-seq data, Qualimap is used in part 5, statistics. It will process the transcripts obtained.

<img src="../../img/PipelineA_RNAseq_Qualimap.png">

**Figure 1 : simplified RNAseq pipeline.**