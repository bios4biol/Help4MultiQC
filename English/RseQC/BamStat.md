## RSeQC

### BAM Statistiques

<img src="../../img/BamStat.png"  width="650" height="400"> 

**Figure 1: values of the different metrics assigned to the various samples.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

In figure 1, the x-axis represents the quantity in either reads or nucleotides per million.

Bam_Stat.py presents all the metrics and values for each sample present in the BAM files. It also calculate the "uniquely mapped reads" metric from mapping quality.

### Metrics

**Total records:** this corresponds to the total number of reads in the BAM file.

**QC failed :** this corresponds to reads that did not pass the quality test. For RNAseq data, the quality test was performed by FastQC, while for SAREK, it is directly performed with the BWA-mem alignment.

**Duplicates :** duplicate reads are reads with the same sequences. For RNAseq data, they can appear during PCR, but also due to the overexpression of a gene. The values can be very high for RNAseq data.

**Non primary hit :** this corresponds to reads that can be either secondary (aligning well to multiple locations in the reference genome) or supplementary (cannot be represented by a linear alignment).

**Unmapped :** this corresponds to unmapped reads.

**Unique :** this corresponds to mapped reads .

**Read-1 :** first read.

**Read-2 :** second read.

**+ve strand :** These are the reads belonging to the forward strand.

**-ve strand :** These are the reads belonging to the reverse strand.

**Non-splice reads :** these are unspliced reads possessing no introns but only exons.

**Splice reads :** these are non-overlapping intron reads.

**Proper pairs :** These are reads that are correctly paired and mapped.

**Different chrom :** This corresponds to reads that are paired with a read present on another chromosome.