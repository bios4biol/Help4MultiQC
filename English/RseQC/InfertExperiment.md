## RSeQC

### Infer experiment

<img src="../../img/rseqc_junction_saturation_plot-1.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1: bar chart representing the portions of reads in the strand orientations.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The y-axis represents the samples and the x-axis the percentage of strand sense assignment for the reads.

infer_experiment.py determines on which strand the reads aligned during the alignment (sense or antisense) based on the strand of the gene to which they aligned.

As shown in Figure 1, the majority of reads aligned to genes present on the antisense strand. The **undetermined** section corresponds to cases where the gene can be read in multiple locations in different orientations. Therefore, it is impossible to determine the strand of the transcribed reads for that gene.

<img src="../../img/paired-end-read-1.jpg">

**Figure 2 : Inner/infer distance schematic.**

*source : [Biostar Issues](https://www.biostars.org/p/9593158/)*