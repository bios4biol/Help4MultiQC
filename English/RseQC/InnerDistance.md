## RSeQC

### Inner distance

<img src="../../img/rseqc_inner_distance_plot-1.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1 : plot of the inner distance's distribution for each sample.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The y-axis represents the number of occurrences and the x-axis the inner distance in base pairs (bp). 

Inner Distance is the distance between 2 paired reads. If the 2 reads overlap, the value will be negative.
It gives an idea of the distance between paired reads, if the inner distance is too large (more than 500 bp) it could be that the reference genome is not the right one and the samples are too different.

It can be used to check fragment size. The results in the figure show an expected distribution. A lower-quality distribution would be much more extensive.

<img src="../../img/paired-end-read-1.jpg">

**Figure 2 : Inner/infer distance schematic.**

*source : [Biostar Issues](https://www.biostars.org/p/9593158/)*