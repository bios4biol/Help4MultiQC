## RSeQC

### Junction Annotation

During the event of splicing mRNA into RNA, introns are spliced out to make way for exons. During these events, the exons then join together. This is what [JunctionAnnotation.py](https://rseqc.sourceforge.net/#junction-annotation-py) will measure.


<img src="../../img/rseqc_junction_annotation_junctions_plot-1.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1 : bar chart representing the junction annotations found in the samples.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The y-axis represents the samples and the x-axis represents the percentage allocation of junction types.

The junctions found thanks to the annotations in the BAM/SAM files may either have already been referenced in the reference genome, or be only partially known, which happens when one of the donor or acceptor sites was not annotated in the reference genome, or be totally new, so none of the donor or acceptor sites was known about the junction.

There are also splicing events that can be annotated and that have the same three categories as the junctions.

For a known genome, it is normal to have many known splicing events or junctions, but there is still a large portion of new or partially new splicing junctions. For humans, we expect values of at least 50% for known junctions. For other species, it depends on the knowledge of the organism.