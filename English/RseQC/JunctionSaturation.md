## RSeQC

### Junction Saturation

<img src="../../img/rseqc_junction_annotation_junctions_plot-1.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1 : graph representing the saturation of splicing junction discovery for each sample.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

This figure shows the number of junctions on the ordinate against the percentage of readings on the abscissa.

The purpose of JunctionSaturation.py is to determine if the sequencing depth was sufficient to obtain a good number of splicing junctions. The first figure provides an indication and shows curves that increase with the size of the sequenced genome. By clicking on one of these curves, you can access figure 2.

<img src="../../img/rseqc_junction_annotation_junctions_plot-1.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 2 : graph representing the saturation of 3 families of splicing junctions for a sample.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

This figure shows the number of junctions on the ordinate against the percentage of readings on the abscissa.

In figure 2, we observe 3 metrics: The first, **All Junctions**, is the sum of the other two metrics. **Known Junctions** indicates all the junctions found in the sequencing that were known from the reference genome. **Novel Junctions** indicates the junctions found that had either the either the receiving part or the accepting part or both splice ends unknown to the reference genome.

We can see that the curve for known junctions is reaching a plateau, meaning that the sequencing depth was appropriate, and that we are close to the maximum measurement of known junctions. On the other hand, the green curve for new junctions is still clearly growing, meaning that greater sequencing depth would enable even more splice junctions to be found. It should be noted, however, that new junctions may be false junctions.