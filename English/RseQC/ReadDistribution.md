## RSeQC 

### Read Distribution

<img src="../../img/rseqc_read_distribution_plot-1.png" width="650" height="400"> 

**Figure 1 : barplot of the read distribution for each samples.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Caption:

**CDS_Exons** refers to the coding part of each exon (excluding the 5’ UTR or 3’ UTR). **5’UTR_Exons** refers to the 5’ UTR part of the exons, and **3’UTR_Exons** refers to the 3’ UTR part of the exons. **Introns** are self-explanatory. **TSS_up_1kb/1kb-5kb/5kb-10kb** are transcription start sites, which are split into three parts: up to 1 kb, 1-5 kb, and 5-10 kb. **TES_down_1kb/1kb-5kb/5kb-10kb** are transcription end sites, which are split into three parts: down to 1 kb, 1-5 kb, and 5-10 kb. **Other_intergenic** contains the centromere and other undefined regions.

For RNAseq data, you should see a majority of CDS and UTR ends. However, it is normal to find introns coming from highly expressed genes.