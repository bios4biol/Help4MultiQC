## RseQC

[RseQC](https://rseqc.sourceforge.net/) is a high-throughput sequencing data processing tool specialized in RNAseq data.

[Here](https://academic.oup.com/bioinformatics/article/28/16/2184/325191) is the link to the article presenting the tool.

It was developed by [Liguo Wang](https://scholar.google.com/citations?user=9sQzL0IAAAAJ), Wei Li and Shengqin Wang.

For RNA-seq data, RseQC is used in part 5, statistics. It will give various transcript and read statistics.

<img src="../../img/PipelineA_RNAseq_RSeQC.png">

**Figure 1 : simplified RNAseq pipeline.**