## RSEM 

### Mapped Reads

The RSEM tool is designed to estimate gene and isoform expression levels from RNAseq data.

<img src="../../img/rsem_assignment_plot-1.png" title="Gene Coverage Profile"> 

**Figure 1 : type's attribution of reads alignments.**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

In the graph above, the samples are comparable to each other and divided into 4 categories:

- **Aligned uniquely to a gene** represents the percentages of reads that are aligned to a single gene.
- **Aligned to multiple genes** represents the percentages of reads that are aligned to multiple genes. 
- **Filtered due to too many alignments** is the percentage of reads that were filtered because they were aligned to too many locations on the reference genomes. 
- **Unalignable reads** refers to the reads that cannot be aligned to the reference.

If you have a high percentage of unalignable reads, it could be due to contamination or a significant difference between the reference genome and the genome of the samples.
