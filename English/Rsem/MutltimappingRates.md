## RSEM 

### Multimapping rates

RSEM is a tool estimating gene and isoform expression levels from RNAseq Data.

<img src="../../img/rsem_multimapping_rates-1.png" width="650" height="400"> 

**Figure 1 : counts of multi-mapping reads for each sample**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

In figure 1, the ordinate shows the number of readings and the abscissa the number of corresponding alignments.
In this example, the overwhelming majority of reads have 1 or 2 alignments, which is a satisfactory result. All reads without alignment correspond to reads that found no match with the reference genome.

The first peak observed here is typically what one would expect for RNAseq data. If other peaks are present, they could be related to sequencing problems. Another possibility is that the genome being studied contains many duplications.
