## RSEM

[RSEM](https://pubmed.ncbi.nlm.nih.gov/21816040/) is a quantification tool for RNAseq data. It can be used with or without a reference genome.

It was developed by [Bo Li](https://aisecure.github.io/) and [Colin N Dewey](https://www.biostat.wisc.edu/~cdewey/).

Here is the link to the issues on [Gitlab](https://github.com/deweylab/RSEM/issues) in case of any problems.

For RNA-seq data, Rsem is used in part 3, quantification.

<img src="../../img/PipelineA_RNAseq_RSEM.png">

**Figure 1 : simplified RNAseq pipeline.**