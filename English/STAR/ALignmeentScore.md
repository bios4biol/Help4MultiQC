## STAR

### Alignment score

<img src="../../img/star_alignment_plot-1.png" title="Alignement stats" width="650" height="400">

**Figure 1 : bar charts representing the reads categorized by their alignment family.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.12.0/results/rnaseq/results-3bec2331cac2b5ff88a1dc71a21fab6529b57a0f/aligner_star_salmon/multiqc/star_salmon/?file=multiqc_report.html)*

In Figure 1, each sample is shown on the ordinate and the percentage of readings on the abscissa.

Metrics :

**Uniquely mapped :** reads that are aligned to only one location in the reference genome.

**Mapped to multiple loci :** reads that are aligned to multiple locations in the reference genome while still being usable for data processing.

**Mapped to too many loci :** reads that are aligned too many times across the reference genome, resulting in a very low-quality score.

**Unmapped too short :** reads that are not aligned because they were too short.

**Unmapped other :** reads that are not aligned because they were not found in the reference genome or because they are of too low quality.

If there are too many multi-mapped sequences, it may be because the reads were too short or the tested genome has many duplications. The graph above shows a good result: we expect at least 90% of reads to be **Uniquely mapped**.