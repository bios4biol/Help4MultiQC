## STAR

[STAR](https://academic.oup.com/bioinformatics/article/29/1/15/272537?login=true) is a RNAseq data aligner made by [Alex Dobin](https://www.cshl.edu/research/faculty-staff/alexander-dobin/)

Here are the [issues](https://github.com/alexdobin/STAR/issues?page=1&q=is%3Aissue+is%3Aopen) on Git where many problems are resolved.

For RNA-seq data, STAR is used in part 2, mapping. It is a reference transcriptome read aligner.

<img src="../../img/PipelineA_RNAseq_STAR.png">

**Figure 1 : simplified RNAseq pipeline.**