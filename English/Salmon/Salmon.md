## Salmon

[Salmon](https://combine-lab.github.io/salmon/) is a transcriptomic quantification tool for RNAseq data.

It was developed by [Rob Patro](http://www.robpatro.com/redesign/) at [Stony Brook University](https://www.cs.stonybrook.edu/), in collaboration with Geet Duggal and Carl Kingsford from the Computational Biology Department at Carnegie Mellon University, as well as Mike Love at UNC Chapel Hill and Rafael Irizarry at Harvard and the Dana-Farber Cancer Institute.

Here is the [article presenting the tool](https://www.nature.com/articles/nmeth.4197). And here is the [documentation](https://salmon.readthedocs.io/en/latest/salmon.html).

For RNA-seq data, Salmon is used as an aligner in part 2 and as a quantifier of reads and transcripts in parts 1 and 3.

<img src="../../img/PipelineA_RNAseq_Salmon.png">

**Figure 1 : simplified RNAseq pipeline.**