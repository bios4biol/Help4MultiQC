# Salmon

### Transcrits quantifications.

<img src="../../img/salmon_plot-1.png"  width="650" height="400"> 

**Figure 1: length distribution of different read fragments.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Figure 1 shows the fragment lengths of paired reads on the x-axis. The ordinate shows the normalized quantities for each sample. These results are related to the graph [inner distance](https://bios4biol.pages.mia.inra.fr/Help4MultiQC/French/RseQC/InnerDistance.html).

By plotting the total size of the two reads plus the inner distance for each association, we can check that our sequencing library is indeed the one specified.

For example, we know that the read size here is 100 bp for two samples (GM_12878 and K562) and 75 bp for the other two (H1 and MCF7). For GM_12878_REP2, with a value of 200, its inner distance is around 0. The two reads occasionally overlap. For H1, on the other hand, we have a peak at 287, so the paired reads hardly overlap at all, or at least much less than for GM_12878.

<img src="../../img/paired-end-read-1.jpg">

**Figure 2 : fragment schematic.**

*source : [Biostar Issues](https://www.biostars.org/p/9593158/)*