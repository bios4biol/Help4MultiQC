## SamTools

### Mapped reads per contig

<img src="../../img/samtools-idxstats-mapped-reads-plot-1.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1 : graph of the proportion of each sample allocated to different chromosomes in normalised counts.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

In Figure 1, chromosomes are indicated on the x-axis and the normalized quantity on the y-axis.

Samtools idxstats provides information on the alignment of sample reads/contigs after their alignment to different parts of the reference genome.

For the human genome, the numbered chromosomes become progressively smaller, so it is normal to see a decreasing trend along the x-axis. The Y chromosome is noted as 0 here, but it is identified, simply at a ratio so low compared to the others (ratio < 0.01%) that it is not represented.

For RNAseq data, the “MT” part corresponds to the mitochondrial genome and can show very high ratios due to the significant expression of these organites.