## SamTools

### Percent mapped

<img src="../../img/samtools_alignment_plot-1.png" width="650" height="400"> 

**Figure 1 : horizontal barplot representing mapping quality.**

*Source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The y-axis represents the different samples and the x-axis represents the percentages of readings according to their mapping quality.

There are three main metrics : 
- **Mapped (with Mapping Quality (MQ)>0)**: This metric corresponds to the number of reads mapped with satisfactory alignment quality.
- **MQ0**: This metric represents the number of reads that could have been mapped, but whose alignment quality is too low (MQ=0).
- **Unmapped**: This metric corresponds to the number of unmapped reads.

A low alignment quality indicates an insufficient confidence probability for this read. Reads with an alignment quality of 0 can mean three different things:

The read is not part of the reference, which can be due to contamination or a little-known reference genome.

The reading is not part of the reference, which may be due to contamination or a non-complete genome. If the alignment quality is really very low, this may also be due to too high a level of multimapping.

Consequently, the majority of your reads should be mapped at around 90% for a well-known genome. In the case of a less well-known reference genome, or when using more distantly related species, this rate may drop to 70/80%.
