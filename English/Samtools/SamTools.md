## SamTools

[SAMtools](https://www.htslib.org/doc/samtools.html) is a tool for processing SAM files, allowing the extraction of various metrics.

[Here](https://pubmed.ncbi.nlm.nih.gov/33590861/) is the link to the latest article on the recent improvements of SAMtools.

It was developed by [Jhon Marshall](https://github.com/jmarshall) and [Petr Danecek](https://www.sanger.ac.uk/person/danecek-petr/)

For RNA-seq data, Samtools is used in part 4, post-mapping processing. It quantifies the BAM/SAM files.

<img src="../../img/PipelineA_RNAseq_SamTools.png">

**Figure 1 : simplified RNAseq pipeline.**