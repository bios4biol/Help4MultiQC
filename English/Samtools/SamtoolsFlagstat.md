## SamTools

### SamTools flagstats

<img src="../../img/Samtools.Flagstat.png" title="Mapped Reads per contig" width="650" height="400"> 

**Figure 1 : values of the different metrics attributed to the different samples.**

*Source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

In figure 1, the x-axis represents the quantity in either readings or nucleotides per million.

To understand these types of metrics, it is essential to understand the concept of a flag. Flags are annotations found in SAM or BAM files, which indicate the alignment status of the reads. For a deeper understanding, I recommend using a dedicated website where you can become more familiar with the different flags :
https://broadinstitute.github.io/picard/explain-flags.html 

### Metrics:
**Total reads** : this is the total number of reads for each sample.

**Total Passed QC** : this is the number of reads that have passed the Quality Control (QC).

**Mapped** : this is the sum of the mapped reads for each sample.

**Secondary Alignments** : secondary alignments correspond to reads aligned to other locations in the reference genome. The primary alignment is the one with the highest alignment score, and the others are considered secondary alignments with the flag 0x100 (Not the primary alignment).

**Duplicates** : duplicates are reads which have the same sequences. For RNAseq data, they can appear during PCR, but also due to overexpression of a gene. The values can be very high for RNAseq data. Duplicates have the flag 0x400 (PCR read or optical duplicate).

**Paired in Sequencing** : this is the number of paired reads, i.e., having the flag 0x1.

**Properly Paired** : these are the reads that are correctly paired and mapped. These reads have the flags 0x1 (paired read) and 0x2 (mapped read), but not the flag 0x4 (unmapped read).

**Self and mate mapped** : these are the reads that are mapped and paired. This metric is less strict than “Properly Paired”. The reads have the flag 0x1 (paired read), but not the flags 0x4 (unmapped read) and 0x8 (unmapped paired read).

**Singletons** : singleton reads are mapped and paired with unmapped reads. They have the flags 0x1 (paired read) and 0x8 (unmapped paired read), but not the flag 0x4 (unmapped read).

**Mate mapped to diff chr** : this is the number of reads that are mapped and paired, but the two reads are located on two different chromosomes. The reads have the flag 0x1 (paired read), but not the flags 0x4 (unmapped read) and 0x8 (unmapped paired read).

**Diff chr (mapQ>=5)** : this is the number of reads that are mapped and paired, but the two reads are located on two different chromosomes while having a good alignment quality. The reads have the flag 0x1 (paired read), but not the flags 0x4 (unmapped read) and 0x8 (unmapped paired read). Their alignment qualities (mapQ) are greater than or equal to 5. 

### Quick analysis :

<img src="../../img/samtools_flagstat.png" width="650" height="400"> 

**Figure 2 : index of good or bad result of the chart.**

*Source : [nf-core eager](https://nf-co.re/eager/2.5.2/docs/output/)*