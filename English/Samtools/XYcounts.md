## SamTools

### Chromosomes X and Y counts

<img src="../../img/samtools-idxstats-xy-plot-1.png" title="Alignement stats" width="650" height="400"> 

**Figure 1 : horizontal barplot representing the proportions of the chromosomes Y or X genes find on the reads.**

*Source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

The graph above represents the proportions of reads in the X and Y chromosomes spread out on the x-axis. This representation is useful for checking that our sample actually comes from the sequenced individual.

In this diagram, we constantly observe the presence of Y chromosome. However, it is important to remain vigilant about the representation of Y, as a part of the Y chromosome is found on the X chromosome. Therefore, we will always have a representation attributed to Y. However, it is the difference in the proportions attributed that allows us to differentiate between males and females.
