# Cutadapt

## Présentation de l'outil

<p style="text-align:justify;"> <a href="https://cutadapt.readthedocs.io/en/stable/"> Cutadapt </a>  est un outil fréquemment utilisé dans le domaine du séquençage à haut débit. Il permet d’éliminer efficacement les adaptateurs, les queues poly-A, les nucléotides de faibles qualités aux extrémités, les N, les séquences de basse qualité et bien d'autres séquences ou nucléotides problématiques.</p>

Il a été développé par l'équipe du [Dr Sven Rahmann](https://www.rahmannlab.de/people/rahmann) à [l'université de Dortmund](https://www.tu-dortmund.de/)

<p style="text-align:justify;"> Si vous rencontrez des difficultés liées à l'utilisation de l'outil Cutadapt, il vous est possible d'ouvrir une issue sur leur <a href="https://github.com/marcelm/cutadapt/issues"> gitlab </a></p>

Dans le pipeline RNAseq,  Cutadapt est lancé par [Trim Galore](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/) qui est un regroupement des outils Cutadapt et FastQC.

<p style="text-align:justify;">Dans le cadre du pipeline Nextflow nf-core/RNA-seq, Cutadapt est lancé en pré-traitement des données, avant l’alignement. Les données sont des lectures d’ARN séquencées par Illumina.</p>

<img src="../../img/Pipeline_RNAseq_TrimGalore.png">

**Figure 1 : pipeline RNAseq simplifié.**
