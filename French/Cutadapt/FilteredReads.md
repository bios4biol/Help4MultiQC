## Cutadapt

### Lectures filtrées

<img src="../../img/cutadapt_filtered_reads_plot-1.png" width="650" height="400"> 

**Figure 1 : diagramme en barre du nombre de lectures passant le filtre cutadapt pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*


<p style="text-align:justify;">Le graphique ci-dessus représente le nombre de lectures ayant passé le filtre sur l'axe des abscisses et les échantillons en ordonnée. Ce sont donc celles dont les adaptateurs ont été retirés. Si une métrique en noir <b> Pairs that were too short </b> apparaît, cela signifie que les lectures de cette métrique n’ont pas passé le filtre car elles étaient trop petites. Il appartient à l’utilisateur de juger si c’est normal ou non d’avoir des données non filtrées. Exemple en figure 2.</p>

<img src="../../img/cutadapt_filtered_reads_plot.png" width="650" height="400"> 

**Figure 2 : diagramme en barre du nombre de lectures passant le filtre cutadapt pour chaque échantillon avec des lectures trop petites.**

*source : [nf-core RNAseq MultiQC](https://training.galaxyproject.org/training-material/topics/transcriptomics/tutorials/ref-based/tutorial.html)*