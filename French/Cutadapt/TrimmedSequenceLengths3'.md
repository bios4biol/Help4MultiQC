## Cutadapt

### Longueurs des séquences découpées en 3'

<p style="text-align:justify;">Dans la partie 3' des lectures, le processus de séquençage, comme celui utilisé par Illumina, peut laisser des adaptateurs dans les lectures. Ces adaptateurs sont ensuite éliminés par Cutadapt.</p>

### Quantifiation :

<img src="../../img/cutadapt_trimmed_sequences_plot_3-1.png"  width="650" height="400">

**Figure 1 : longueurs des adaptateurs découpés par Cutadapt pour chaque lecture de chaque échantillon représenté par une couleur spécifique.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">Dans la figure 1, chaque courbe représente un échantillon à l'aide d'une couleur différente pour chaque échantillon. Il est difficile de distinguer les échantillons car les courbes se superposent. Ce graphique correspond exactement à ce que l’on attend de Cutadapt. Une grande quantité d’extrémités 3’ courtes (jusqu’à 5 bases) sont nettoyées. La forme de la courbe dépends de la position des adaptateurs. Par exemple pour les sARN, l'adaptateur peut être situé au milieu de la lecture. Dans ce cas là, la longueur coupée est plus grande que 10 bases. </p>

### Observées/Attendues :

<img src="../../img/cutadapt_trimmed_sequences_plot_3-1(1).png" width="650" height="400">

**Figure  2 : longueurs observées/longueurs attendues des adaptateurs découpés par Cutadapt pour chaque lecture de chaque échantillon représenté par une couleur spécifique.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">Le graphique présenté dans la Figure 2 a comme ordonnée le ratio des données observées par rapport aux données attendues. Les données attendues sont calculées par Cutadapt. Dans ce graphique, chaque échantillon est représenté par une couleur, et nous observons des disparités au centre de la figure qui se traduisent par différents pics de valeurs. Ces pics illustrent les différences entre les valeurs attendues et observées. Même si ces différences peuvent sembler préoccupantes, il est important de noter que les valeurs inattendues autour de 13 à 63 bases sont très petites, tout comme les valeurs attendues (on parle d’un effectif de moins de 1000 lorsqu’on compare avec les 30 millions de la figure 1). Les valeurs sont donc à comparer à la figure 1 pour voir s’il y a des pics avec un grand effectif.</p>