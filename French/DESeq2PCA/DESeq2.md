## DESeq2

[DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html) est disponible dans [Bioconductor](https://bioconductor.org/)et est maintenu par Michael Love. Il s'agit d'une librairie R qui mesure les différences d'expression génique des données de comptage issues de séquençage à haut débit.

<p style="text-align:justify;">Elle permet notamment de réaliser des ACP entre échantillons pour mettre en évidence d'éventuelles ressemblances entre échantillons (Les réplicats se retrouvent souvent proche).</p>

Ici le [manuel de référence](https://bioconductor.org/packages/3.19/bioc/manuals/DESeq2/man/DESeq2.pdf), et un exemple de traitement de données [RNAseq](https://bioconductor.org/packages/3.19/bioc/vignettes/DESeq2/inst/doc/DESeq2.html).

<p style="text-align:justify;">Dans le cadre du pipeline Nextflow nf-core/RNAseq, DESeq2 est lancé dans la "partie 5 : statistiques". Les données d'entrée de DesEQ2 est une matrice de comptage comprenant la liste des transcrits alignés avec leur quantification, pour chaque échantillon. L'objectif est d'analyser l'expression différentielle selon les échantillons.</p>

<img src="../../img/Pipeline_RNAseq_DESeq2.png">

**Figure1 : pipeline RNAseq simplifié.**
