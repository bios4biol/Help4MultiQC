## DESseq2

### ACP : Analyse en Composante Principale

<img src="../../img/salmon_deseq2_pca-plot.png" width="650" height="400"> 

**Figure 1:  ACP générée à partir de la matrice de quantification RNAseq, calculée par RSEM.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">Les ACP sont de bons outils pour valider les échantillons biologiques avec la vérification d'une bonne séparation en lots. Sur l’exemple ci-dessus, chaque point représente un échantillon. Les points sont regroupés par deux car chaque échantillon est représenté à proximité de son réplicat. C’est un bon indicateur que les étapes expérimentales se sont bien passées.</p>


<img src="../../img/DESeq2PLOT.png"> 

**Fidure 2: differences in principal components between sample prefix (origin or replicate).**

*source : [nf-core RNAseq Output](https://nf-co.re/rnaseq/3.14.0/docs/output/#deseq2)*
