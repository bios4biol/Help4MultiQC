## DESseq2

### Heatmap

<img src="../../img/star_rsem-plot.png"  width="650" height="400"> 

**Figure 1: heatmap des similarités entre échantillons basée sur les écarts des distances euclidiennes. La matrice de comptage utilisée est issue d'un mapping STAR puis d'une quantification RSEM.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">La heatmap représente les valeurs comparatives obtenues après le calcul des distances euclidiennes dans l’ACP précédente. Ici, plus les carrés sont bleus, plus les échantillons sont proches ; plus ils sont rouges, plus ils sont éloignés. Ce graphique permet d'observer les différences entre échantillons.</p>