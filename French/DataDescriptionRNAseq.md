# Description des données

Les schémas et graphiques présentés dans ce GitBook proviennent, en grande partie, de la documentation des pipelines Nextflow nf-core.

## Données RNAseq

Les données RNAseq sont issues du pipeline [Nextflow nf-core RNAseq](https://nf-co.re/rnaseq/3.14.0/):

<img src="../img/nf-core-rnaseq_metro_map_grey.png" width="700" height="350">

**Figure 1 : metro map des différents outils et workflows intégrés au pipeline RNAseq.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Le pipeline RNAseq peut suivre différentes voies selon la question biologique ou les données d'entrées. Le guide décrit la plupart des graphiques disponibles, mais les conclusions de ces graphiques peuvent varier en fonction du pipeline ou de la question biologique posée.

*source : [RNAseq data ](https://github.com/nf-core/test-datasets/blob/rnaseq/README.md#full-test-dataset-origin)*

Les données utilisées sont des données pairées du génome humain, obtenues par séquençage Illumina HiSeq 2000 (GM12878 et K562) ou Illumina Genome Analyzer IIx (pour MCF-7 et H1-hESC). Elles ont été employées dans un projet global de [séquençage humain](https://pubmed.ncbi.nlm.nih.gov/22955616/), et dans un projet de mesure de qualité de pipeline quantitative RNAseq. Comme il s'agit de données issues du génome humain, alors nous nous attendons à obtenir un grand nombre de données dupliquées.

Les séquençages des quatre échantillons ont été répliqués trois fois chacun. Tous les échantillons proviennent d’*Homo sapiens*.



