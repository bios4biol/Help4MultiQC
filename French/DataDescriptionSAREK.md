# Description des données

Les schémas et graphiques présentés dans ce GitBook proviennent, en grande partie, de la documentation des pipelines Nextflow nf-core.

## Données SAREK

*source : [SAREK pipeline results ](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/)*


Les échantillons utilisés sont des données pairées du génome humain, obtenues par séquençage Illumina HiSeq 1500 ([SRR7890918(HCC1395T)](https://www.ncbi.nlm.nih.gov/sra/?term=SRR7890918) et [SRR7890919(HCC1395N)](https://www.ncbi.nlm.nih.gov/sra/?term=SRR7890919)). Ces données sont issues du séquençage clinique de tumeurs.


<img src="../img/sarek_workflow.png" width="400" height="700">

**Figure 1 : pipeline SAREK**

Le pipeline SAREK a pour but de détecter les variants dans les séquences, lors de leur comparaison avec un génome de référence.