## DupRadar

[Dupradar](https://bioconductor.org/packages/release/bioc/html/dupRadar.html) est un outil qui permet d’analyser le taux de duplication présent dans chaque échantillon.

Il a été développé par [Sergi Sayols](https://www.uu.se/en/contact-and-organisation/staff?query=N19-1558), voici le [lien](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1276-2) vers l'article de publication.

<p style="text-align:justify;">Dans le cadre du pipeline Nextflow nf-core/RNAseq, DupRadar est lancé dans la "partie 5 : statistiques". Les données analysées par DupRadar sont les lectures alignées afin d'estimer le niveau de duplication pour chaque échantillon.</p>

<img src="../../img/Pipeline_RNAseq_dupRadar.png">

**Figure 1 : pipeline RNAseq simplifié.**