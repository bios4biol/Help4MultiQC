## DupRadar

### Résultats graphiques

<img src="../../img/dupradar_multiplot.png" width="650" height="400">

**Figure 1 :  un cas sans duplication à gauche et un cas avec duplications à droite**

*source : [nf-core RNAseq description](https://pilm-bioinformatics.github.io/pipelines-nf-rnaseq/output.html)*


<p style="text-align:justify;">Dans la figure 1, les graphiques représentent en ordonnée le taux de lectures dupliquées et en abscisse le niveau d'expression exprimé en lectures/kbp (1000 paires de bases).</p>
<p style="text-align:justify;">Ces graphiques en forme de droites de régression illustrent parfaitement les 2 cas possibles. Dans le cas d’un séquençage classique, plus la profondeur de séquençage est grande, plus nous aurons naturellement des duplicatas. Dupradar permet de visualiser les niveaux de duplicatas obtenus en fonction de l’expression trouvée pour chaque échantillon. Dans un cas idéal, le taux de duplicatas augmente soudainement, mais reste faible (moins de 10%) à des niveaux d’expression bas. En revanche, dans un cas où il y a un problème de duplication, les points représentent plutôt une forme uniforme d’œufs, ce qui se traduit par une courbe de régression croissante de manière continue. Dans le deuxième cas, ce n’est pas la profondeur de séquençage mais le niveau de duplication de la séquence qui est à l’origine du nombre de duplicatas.</p>

<img src="../../img/dupradar-plot-1.png"  width="750" height="400">

**Figure 2 : modèles linéaires généralisés de Dupradar modélisant l'expression des données dupliquées pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">Les valeurs présentées dans la Figure 2 sont issues de données RNAseq. On observe un niveau de duplication très élevé, en particulier pour les quatre échantillons situés dans la partie supérieure du graphique. Ceci peut être typique pour des données RNAseq, étant donné que certains gènes, tels que les gènes mitochondriaux peuvent être beaucoup plus exprimés que d’autres. Il y a aussi l’ARN ribosomique qui est un gène très dupliqué dans le génome. Cela fait que l’on retrouve cette séquence de nombreuses fois dans le transcriptome, ce qui peut donner lieu à du multimapping et des duplications. Il est donc très difficile de mesurer l’expression différentielle de l’ARN ribosomique.</p>