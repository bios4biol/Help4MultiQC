## FastQC

### Contenu des adaptateurs

Si vos lectures contiennent très peu d’adaptateurs, vous verrez le message suivant :

<img src="../../img/CaptureAdapterAllGood.PNG" title="moins de 1 pour cent d’adaptateurs dans les lectures"/>

<p style="text-align:justify;">Si toute la courbe est dans la zone verte cela veut dire que le contenu en adaptateurs est faible pour ces échantillons. Si vous cliquez sur la courbe, vous trouverez des informations sur la quantité d’adaptateurs à la position sélectionnée et le type d’adaptateur détecté.</p>

<img src="../../img/fastqc_adapter_content.png" title="good adapter content"/>

**Figure 1 : bon résultat du pourcentage de lectures ayant des adaptateurs pour une position données (en pb).**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">Si vous avez des courbes dans le rouge alors cela veut dire que les échantillons correspondants contiennent une très grande quantité d’adaptateurs. Le pourcentage de séquençage contenant des adaptateurs est représenté sur l’axe des y et sur l’axe des x, c’est la position sur la lecture. Dans l’exemple ci-dessous, la courbe noire signifie qu’à partir de la 10ème base de la lecture, environ 65% des lectures sont des adaptateurs.</p>

<img src="../../img/fastqc_adapter_content_plot_bad.png" title="bad adapter content"/>

**Figure 2 : mauvais résultat du pourcentages de lectures ayant des adaptateurs pour une position données (en pb).**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">Faites attention à utiliser les barres de défilement pour aller jusqu’à la fin des lectures à droite lorsqu’elles sont relativement longues.</p>

<img src="../../img/fastqc_adapter_contentex.png"/>

**Figure 3 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*

### Avertissement

Ce module émettra un avertissement si une séquence d'adaptateurs est présente dans plus de 5% de toutes les lectures.

### Échec

Ce module émettra un avertissement si une séquence d'adaptateurs est présente dans plus de 10% de toutes les lectures.

### Les avertissement les plus courants

<p style="text-align:justify;">Toute librairie où une proportion raisonnable des tailles d’insertion est plus courte que la longueur de lecture déclenchera ce module. Cela n’indique pas un problème en soi - juste que les séquences devront être coupées avant de procéder à toute analyse en aval.</p>