## FastQC

### Statistiques générales

<p style="text-align:justify;">En haut de chaque rapport MultiQC se trouve le tableau des “Statistiques Générales”. Celui-ci montre un aperçu des valeurs clés, issues de chaque outil du pipeline. L’objectif du tableau est de rassembler les statistiques d'analyse de chaque échantillon afin de pouvoir les visualiser en un seul endroit.</p>

<img src="../../img/generalStatistics.PNG" title="general Statistics illustration"/>

**Figure 1 : statistiques générale montré en sortie de MultiQC.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">En passant la souris sur les en-têtes de colonne dans la sortie (HTML multiQC pas dans le guide Book) il s'affiche une description détaillée du type de données et de l'outil qui les a produites. Cliquer sur un en-tête trie le tableau par cette valeur. Cliquer à nouveau change la direction du tri. Vous pouvez maintenir la touche Maj enfoncée et cliquer sur plusieurs en-têtes pour trier par plusieurs colonnes.</p>

### Configurer les colonnes

<p style="text-align:justify;">Cliquer sur le bouton “Configurer les colonnes”, situé au-dessus du tableau, permettra d'ouvrir une autre fenêtre
avec des informations plus détaillées sur chaque colonne, ainsi que des options pour afficher/masquer et changer l’ordre des colonnes.</p>

<img src="../../img/columns.PNG" title="choose columns to display"/>

### Graphique des données

Le bouton “Plot” permet de tracer les valeurs de deux colonnes l’une contre l’autre, pour un même échantillon.

<img src="../../img/plotDataTable.PNG" title="plot data table in general statistics section"/>

**Figure 2 : comparatif de deux colonnes ( taux de GC et taux de duplications) pour chaque échantillon.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

> Le bouton “Copier le tableau” copie le tableau au format tabulaire, vous pouvez coller le contenu dans un fichier texte ou tabulaire.

### Avertissement

N’oubliez pas de faire défiler vers la gauche pour voir toutes les colonnes affichées.