# FastQC

## Lectures surreprésentées

<p style="text-align:justify;">Sur ce graphique, vous verrez le nombre de lectures surreprésentées pour chaque échantillon. Pour savoir de quelle lecture il s’agit, vous devrez ouvrir le graphique fastQC. Seuls les échantillons avec plus de 0,1% de lectures surreprésentées sont représentés. En bleu, le graphique montre les lectures partagées par la plupart des échantillons. En noir, le graphique représente les autres lectures surreprésentées.</p>

![Example overrepresented sequences](../../img/OverepresentedSequenceNotOK.PNG "overrepresented sequence") 

**Figure 1 : diagramme en barre représentant le nombre de lectures surreprésentées.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

### Avertissement

Ce module émettra un avertissement si une lecture représente plus de 0,1% du total.

### Echec

Ce module émettra une erreur si une lecture représente plus de 1% du total.

### Les avertissement les plus courants
<p style="text-align:justify;">Ce module sera souvent déclenché lorsqu’il est utilisé pour analyser des librairies de petits ARN où les lectures ne sont pas soumises à une fragmentation aléatoire, et la même lecture peut naturellement être présente dans une proportion significative de la librairie.</p>

Pour le séquençage Novaseq, si la lecture est dégradée ou trop courte (souvent le cas pour la lecture 2), vous obtiendrez ici des lectures `GGGGGGGGGGGGGGGGGGGGGGGGGGGGG` ou `TTTTTTTTTTTTTTTT` dans la partie des lectures surreprésentées du rapport fastqQC.

<p style="text-align:justify;">Si un transcriptome dans RNASeq est très fortement exprimé, vous pouvez également en voir une partie dans le rapport fastQC. Dans ce cas, vous pouvez faire une analyse blast pour vérifier de quel gène il s’agit.</p>