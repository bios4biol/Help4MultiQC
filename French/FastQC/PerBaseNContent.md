# FastQC

## Contenu en N par base

<p style="text-align:justify;">Certains séquenceurs substituent un N lorsqu’ils ne peuvent pas faire un appel de base avec une confiance suffisante. Ce graphique montre le pourcentage d’appels de base N à chaque position.</p>

<img src="../../img/NContent.PNG" title="N content plot"/>

**Figure 1 : pourcentage de N dans les lectures.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">Une faible proportion de N à la fin de la séquence n’est pas une cause d’inquiétude. Il y a souvent plus de N sur la lecture 2 que sur la lecture 1, ce qui est normale car la lecture 1 est souvent de meilleur qualité. Si la proportion devient trop élevée, cela indique par exemple une dégradation du matériel séquencé.</p>

Les séquenceurs Novaseq n’utilisent plus de N. En revanche, les MiSeq et HiSeq d’Illumina peuvent substituer des N.

<img src="../../img/fastqc_per_base_n_content.png"/>

**Figure 2 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*