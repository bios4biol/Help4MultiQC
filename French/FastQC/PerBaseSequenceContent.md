# FastQC

## Contenu en séquence par base

Analyse par base le long de chaque séquence.

![PerBaseSeqContentheatmap](../../img/PerBaseSeqContent_heatmap.png "Overview heatmap")

**Figure 1 : heatmap des échantillons indiquant la composition en nucléotides.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

Cliquer sur un échantillon permet de visualiser les bases le long de chaque séquence :

![PerBaseSeqContentoneSample](../../img/PerBaseSeqContent_oneSample.png "Analysis for one sample")

**Figure 2 : pourcentages de A, T, C ou G dans les lectures pour un échantillon sélectionné sur la figure 1.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">Chacune des 4 bases d’ADN se trouve normalement avec à peu près le même pourcentage (peu ou pas de différence entre les bases) au fur et à mesure que la séquence est lue.</p>

<p style="text-align:justify;">Par conséquent, les lignes sur le graphique devraient être parallèles les unes aux autres. La quantité relative de chaque base devrait refléter la quantité globale de ces bases dans votre génome, mais en aucun cas il ne devrait y avoir de grands déséquilibres entre elles.</p>

<p style="text-align:justify;">S’il y a un déséquilibre entre les différentes bases, cela indique généralement qu’une séquence est surreprésentée et que votre librairie est donc contaminée. Si ce biais est constant sur toutes les bases, cela indique :</p>

   -  soit que la librairie originale était biaisée,

   -  soit qu'il y a un problème systématique lors du séquençage des lectures

### Avertissement

Le module émet un avertissement si la différence entre A et T, ou U et C est supérieure à 10% à n’importe quelle position.

### Échec

Ce module échoue si la différence entre A et T, ou U et C est supérieure à 20% à n’importe quelle position.

### Les avertissement les plus courants

![Example of good data](../../img/PerBaseSeqContent_good.png "Good data")

**Figure 3 : exemple de bon résultat.**

Bonnes données :
- Lignes parallèles sur toute la longueur : les lignes sont parallèles les unes aux autres.
- Dépendant de l’organisme (contenu en GC)

![Example of bad data](../../img/PerBaseSeqContent_bad.png "Bad data")

**Figure 4 : exemple de mauvais résultat.**

Mauvaises données :
- Séquencence avec des biais de positions.


<img src="../../img/fastqc_per_base_sequence_content.png"/>

**Figure 5 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*