# FastQC

## Contenu en GC par séquence

Représentation du contenu en GC pour chaque position de lecture.

<p style="text-align:justify;">Dans une  librairie aléatoire, vous pouvez vous attendre à ce qu’il y ait peu ou pas de différence entre les différentes bases dans une lecture, donc la ligne tracée devrait être à peu près horizontale. Le contenu global en GC devrait refléter le contenu en GC du génome sous-jacent.</p>

<p style="text-align:justify;">Si vous voyez un biais qui change pour différentes bases, cela pourrait indiquer une séquence surreprésentée et donc une contamination de votre  librairie. Un biais constant sur toutes les positions indique soit que la  librairie originale était biaisée, soit qu’il y a eu un problème systématique lors du séquençage de la  librairie.</p>

![Exemple de bonnes données pour le contenu en GC](../../img/GCcontent.png "GC content") 

**Figure 1 : pourcentage de GC dans les lectures à une position donnée.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

### Avertissement

Ce module émet un avertissement si le contenu en GC s’écarte de plus de 5% du contenu moyen en GC.

### Échec

Ce module échouera si le contenu en GC s’écarte de plus de 10% du contenu moyen en GC.

### Les avertissement les plus courants

<p style="text-align:justify;">La ligne de ce graphique devrait être horizontale sur tout le graphique. Un biais ponctuel est une séquence surreprésentée qui contamine votre  librairie. Un biais constant sur toutes les bases indique soit une  librairie originale biaisée, soit un problème lors du séquençage de la  librairie.</p>

![Exemple de mauvaises données pour le contenu en GC](../../img/GCcontent_good.png "Good data")

**Figure 2 : exemple de bon résultat.**

Bonnes données : distribution lisse et régulière, aucune variation sur la séquence de lecture.

![Example of good data for GC content](../../img/GCcontent_bad.png "Bad data")

**Figure 3 : exemple de mauvais résultat.**

Mauvaises données :

- Variation sur la séquence de lecture.
- Un pic autour de 40% indique que les lectures avec un contenu en GC de 40% sont surreprésentées dans votre échantillon.

<img src="../../img/fastqc_per_sequence_GC_content.png"/>

**Figure 4 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*