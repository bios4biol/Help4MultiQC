# FastQC

## Scores de qualité par séquence

![Per Sequence Quality Score](../../img/PerSequenceQualityScore.png "Per Sequence Quality Score") 

**Figure 1 : score moyen du phred score pour chaque lecture pour chaque échantillon.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">Ce graphique vous permet d’évaluer la qualité moyenne de vos séquences. Si la qualité moyenne de la lecture impacte un petit pourcentage de séquences, la distribution sera correcte (taux d’erreur acceptable). Sinon, cela signifie qu’il y a un problème spécifique que vous devrez déterminer.</p>

### Avertissement

Un avertissement est émis si la qualité moyenne la plus fréquemment observée est inférieure à 27 - équivalent à un taux d’erreur supérieur à 0,2%.

### Échec

Une erreur est déclenchée si la qualité moyenne la plus fréquemment observée est inférieure à 20 - équivalent à un taux d’erreur supérieur à 1%.

### Les avertissement les plus courants

![Per Sequence Quality Score bad data](../../img/fastqc_per_sequence_quality_score.png "Per Sequence Quality Score") 

**Figure 2 : schéma des résultats attendues.**