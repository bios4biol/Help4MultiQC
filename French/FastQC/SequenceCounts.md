# FastQC

## Comptage des séquences

Ce module résume le comptage des séquences uniques et dupliquées pour chaque échantillon.

<img src="../../img/fastqc_sequence_counts_plot.png" title="sequence_counts_plot" height="400"/>

**Figure 1 : diagramme en barres représentant le pourcentage de lectures uniques et dupliquées pour chaque échantillon.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

Les comptages de lectures dupliquées sont uniquement des estimations et sont définis dans la section [Niveaux de duplication des séquences](https://bios4biol.pages.mia.inra.fr/Help4MultiQC/French/FastQC/SequenceDuplicationLevels.html).


<img src="../../img/fastqc_sequence_counts.png"/>

**Figure 2 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*