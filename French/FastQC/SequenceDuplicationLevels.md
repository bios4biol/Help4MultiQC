# FastQC

## Niveaux de duplication des lectures

Cet outil compte le degré de duplication pour chaque échantillons et crée un graphique montrant le nombre relatif de lectures avec différents degrés de duplication.

Il identifie les *lectures dupliquées* s’il y a une correspondance parfaite de lecture le long des 75 premières bases.

<p style="text-align:justify;">Le graphique montre la proportion des échantillons dans chacun des différents niveaux de duplication. Il y a deux lignes sur le graphique. La ligne bleue prend l’ensemble complet de lectures et montre comment ses niveaux de duplication sont distribués. Dans le graphique rouge, les lectures sont dédupliquées et les proportions montrées sont les proportions de l’ensemble dédupliqué provenant de différents niveaux de duplication dans les données originales.</p>



<img src="../../img/FastQC_duplication_levels.png" title=" duplication levels" height="400"/>

**Figure 1 : niveaux de duplications pré-déduplication et post-déduplication.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

Dans le graphique multiQC, seule la ligne bleue est tracée comme illustré dans la figure suivante.

<img src="../../img/fastqc_sequence_duplication_levels_plot.png" title="multiQC duplication levels" height="400"/>

**Figure 2 : niveaux de duplications des lectures pour chaque échantillon.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

Ce graphique peut aider à identifier les échantillons à faible complexités, ce qui pourrait résulter de :

- trop de cycles d’amplification PCR, ou
- trop peu de matériel de départ en quantité et/ou en diversité

<p style="text-align:justify;">Dans des échantillons diversifiés, la plupart des lectures n’apparaîtront qu’une seule fois dans l’ensemble final. Un faible niveau de duplication peut indiquer un très haut niveau de couverture de la lecture cible, mais un haut niveau de duplication est plus susceptible d’indiquer un certain type de biais d’enrichissement (par exemple, une sur-amplification PCR).</p>

<p style="text-align:justify;">Dans des échantillons correctement diversifiés, la plupart des lectures devraient se situer à l’extrême gauche du graphique dans les lignes rouge et bleue. Un niveau général d’enrichissement, indiquant un sur-séquençage large des échantillons en questions, aura tendance à aplatir les lignes, abaissant l’extrémité basse et augmentant généralement les autres catégories. Des enrichissements plus spécifiques de sous-ensembles, ou la présence de contaminants à faible complexité, auront tendance à produire des pics vers la droite du graphique. Ces pics de duplication élevée apparaîtront le plus souvent dans la trace bleue car ils représentent une proportion élevée dans les échantillons, mais disparaissent généralement dans la trace rouge car ils représentent une proportion insignifiante de l’ensemble dédupliqué. Si les pics persistent dans la trace bleue, cela suggère qu’il y a un grand nombre de lectures hautement dupliquées différentes, ce qui pourrait indiquer soit un ensemble de contaminants, soit une duplication technique très sévère.</p>

### Avertissement

Ce module émettra un avertissement si les lectures non uniques représentent plus de 20% du total.

### FÉchec

Ce module émettra une erreur si les lectures non uniques représentent plus de 50% du total.

### Les avertissement les plus courants

<p style="text-align:justify;">L’hypothèse sous-jacente de ce module est celle d’une librairie diversifiée non enrichie. Toute déviation de cette hypothèse générera naturellement des duplications et peut entraîner des avertissements ou des erreurs de ce module.</p>

<p style="text-align:justify;">En général, il existe deux types potentiels de duplicats dans les lectures, les <b>duplicats techniques</b> résultant d’artefacts PCR, ou les <b>duplicats biologiques</b> qui sont des collisions naturelles où différentes copies de la même lecture sont sélectionnées au hasard. Du point de vue de la lecture, il n’y a aucun moyen de distinguer ces deux types et les deux seront signalés comme des duplicats ici.</p>

<p style="text-align:justify;">Un avertissement ou une erreur dans ce module est simplement une déclaration que vous avez épuisé la diversité dans au moins une https://nf-co.re/eager/2.5.1/docs/output/partie de votre librairie et que vous re-séquencez les mêmes séquences. Dans des échantillons supposément diversifiés, cela suggérerait que la diversité a été partiellement ou complètement épuisée et que vous gaspillez donc la capacité de séquençage. Cependant, dans certains types d'échantillons, vous aurez naturellement tendance à sur-séquencer des parties dezs échantillons et donc à générer des duplications et vous attendrez donc à voir des avertissements ou des erreurs de ce module.</p>

<p style="text-align:justify;">Dans les <b>librairies RNA-Seq</b>, les lectures de différents transcrits seront présentes à des niveaux très différents dans la population de départ. Afin de pouvoir observer les transcrits faiblement exprimés, il est donc courant de sur-séquencer fortement les transcrits hautement exprimés, ce qui peut potentiellement créer un grand ensemble de duplicats. Cela entraînera une duplication globale élevée dans ce test et produira souvent des pics dans les niveaux de duplication plus élevés. Cette duplication proviendra de régions physiquement connectées, et un examen de la distribution des duplicats dans une région génomique spécifique permettra de distinguer le sur-séquençage de la duplication technique générale, mais ces distinctions ne sont pas possibles à partir des fichiers fastq bruts. Une situation similaire peut se produire dans les librairies ChIP-Seq hautement enrichies, bien que la duplication y soit moins prononcée.</p>

<p style="text-align:justify;">Enfin, si vous avez une librairie où les points de départ des lectures sont contraints (une librairie construite autour de sites de restriction par exemple, ou une librairie de petits ARN non fragmentée), alors les sites de départ contraints généreront des niveaux de duplication énormes qui <b>ne doivent pas être traités comme un problème</b>, <b>ni supprimés par déduplication</b>. Dans ces types de librairies, vous devriez envisager d’utiliser un système tel que le <b>codage aléatoire</b> (UMI) pour permettre la distinction des duplicats techniques et biologiques.</p>

Pour plus d'informations, consultez [cet article](https://sequencing.qcfail.com/articles/libraries-can-contain-technical-duplication/).

<img src="../../img/fastqc_sequence_duplication_level.png"/>

**Figure 3 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*