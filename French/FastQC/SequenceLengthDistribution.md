# FastQC

## Distribution de la longueur des lectures

<p style="text-align:justify;">Certains séquenceurs à haut débit génèrent des lectures  de longueur uniforme, mais d’autres peuvent contenir des lectures de longueurs très variées. Même au sein de librairies de longueur uniforme, certains pipelines vont nettoyer les lectures pour éliminer les bases de mauvaise qualité en fin de celles-ci.</p>

Ce module génère une distribution des tailles des lecturess

<img src="../../img/fastqc_sequence_length_distribution_plot.png" alt="sequence length" height="400"/>

**Figure 1 : distribution des tailles des lectures.**

<p style="text-align:justify;">Dans de nombreux cas, cela produira un graphique simple montrant un pic à une seule taille, mais pour les fichiers FastQ de longueur variable, le graphique permettra de visualiser les quantités de lectures ayant des tailles différentes.</p>

### Avertissement

Ce module émettra un avertissement si toutes les lectures n’ont pas la même longueur.

### Échec

Ce module émettra une erreur si l’une des lectures a une longueur nulle.

###  Les avertissement les plus courants

<p style="text-align:justify;">Plusieurs situations peuvent conduire à des longueurs de lecture non uniques (coupe, type de librairie, séquençage…), donc la même observation peut être à la fois attendue et susciter des inquiétudes concernant le type de librairie, le type de séquençage et le prétraitement des données appliqué.</p>

<p style="text-align:justify;">Pour certaines plateformes de séquençage, il est tout à fait normal d’avoir des longueurs de lecture différentes, donc les avertissements ici peuvent être ignorés. Lorsque les lecturess ont été coupées pour éliminer les bases de mauvaise qualité ou les adaptateurs, cela entraîne des avertissements qui peuvent également être ignorés.</p>

À titre d’exemple, le graphique suivant donne des profils attendus pour les lectures nettoyées de petits ARN.

<img src="../../img/fastqc_sequence_length_cutadapt_smallrna.png" alt="sequence length smallRNA" height="400"/>

**Figure 1 : longueur des parties des lectures coupées pour chaque échantillon.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">Les différents pics correspondent à des biotypes de petits ARN. Par exemple, les miARN correspondent au pic positionné à 22 pb. Mais pour les librairies mRNAseq, ce type de profil peut être préoccupant.</p>