# FastQC

## Histogramme représentant de qualité des séquences

Ce graphique représente la qualité moyenne des lectures à chaque position pour 2 échantillons (représentés par les 2 courbes).

<img src="../../img/fastqc_per_base_sequence_quality_plot.png" title="sequence Quality Histogram" height="400"/>

**Figure 1 : qualité moyenne des lectures à chaque position pour 2 échantillons.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

<p style="text-align:justify;">L’axe des y sur le graphique représente le scores de qualité. Plus le score est élevé, meilleure est la qualité de la base. L’arrière-plan du graphique divise l’axe des y en appels de très bonne qualité (vert), appels de qualité raisonnable (orange) et appels de mauvaise qualité (rouge). La qualité des appels sur la plupart des plateformes se dégrade au fur et à mesure que la lecture progresse, il est donc courant de voir des appels de base tomber dans la zone orange vers la fin de la séquence.</p>

### Avertissement

Un avertissement sera émis si le quartile inférieur pour une base est inférieur à 10, ou si la médiane pour une base est inférieure à 25.

### Échec

Ce module émettra une erreur si le quartile inférieur pour une base est inférieur à 5 ou si la médiane pour une base est inférieure à 20.

### Les avertissement les plus courants

<p style="text-align:justify;">Les avertissements et les échecs les plus fréquents sont dus à la dégradation en fin de lectures, surtout lorque les séquences sont longues. Vous pouvez constater que la qualité générale de la lecture tombe juste au niveau d'un avertissement où une erreur est déclenché.</p>

<p style="text-align:justify;">Si la qualité des lectures est trop basse, il faut les nettoyer pour ne conserver que les lectures de bonnes et moyennes qualités. Ces dégradations sont généralement liées à un mauvais nettoyage des adaptateurs.</p>

<p style="text-align:justify;">Un avertissement ou une erreur peuvent être émis pour une baisse temporaire de la qualité, sans conséquence ensuite pour la qualité de la séquence. Cela peut se produire s’il y a un problème transitoire avec la lecture (des bulles passant à travers une cellule de flux par exemple). Dans ces cas, le nettoyage n’est pas conseillé car il supprimerait la suite de la séquence qui peut être une bonne séquence. Par contre, il est possible de masquer ces bases lors du mapping ou de l’assemblage ultérieur.</p>

<p style="text-align:justify;">Si votre échantillon a des lectures de longueurs variables, alors un avertissement/erreur peut être déclenché vu la couverture très faible de certaines zones. Avant d'envisager un nettoyage, regardez la distribution des séquences pour vérifier l'étendue de la zone pas assez couverte.</p>

<img src="../../img/fastqc_sequence_quality_histogram.png"/>

**Figure 2 : schema représentant les résultats attendus.**

*source : [eager pipeline nf-core](https://nf-co.re/eager/2.5.1/docs/output/)*