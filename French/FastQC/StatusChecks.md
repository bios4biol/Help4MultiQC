# FastQC

## Vérification du statut

Ce module résume les résultats de statut pour toutes les analyses FastQC sous forme de carte thermique illustrant **Réussite** (vert), **Avertissements** (orange) et **Échecs** (rouge).

L’image suivante illustre un modèle de statut classique pour une analyse RNAseq.

<img src="../../img/fastqc-status-check-heatmap.png" title="fastqc-status-check-heatmap" height="400"/>

**Figure 1 : résultats des étapes FastQC pour tous les échantillons.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*
