# FastQC

## Dépannage

### Qualité du séquençage

La qualité du séquençage n’est pas nécessairement correcte. C’est la raison pour laquelle certains logiciels d’alignement recalculent la qualité : GATK.

### Biais technique

#### Biais dû à l’amorçage aléatoire par hexamère

Ce biais impacte la composition du début des séquences.

![image](../../img/hexamer.png "Hexamer random priming") 

**Figure 1 : pourcentages de A, T, C ou G dans les lectures pour un échantillon.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*

#### Biais dû à une contamination

Ce graphique est un exemple de biais de pourcentage de GC qui pourrait indiquer une contamination de l’échantillon par un autre organisme ou un fragment.

![image](../../img/contamination.png "Bias due to contamination") 

**Figure 2 : graphique représentant le pourcentage de GC dans les lectures pour un échantillion et selon une distribution gaussienne.**

*source : [Babraham Training Courses](https://www.bioinformatics.babraham.ac.uk/training.html)*