# Logiciel FastQC

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) est un logiciel développé par le groupe [Babraham Bioinformatics](https://www.bioinformatics.babraham.ac.uk/) de [l'institut Babraham](https://www.babraham.ac.uk/).

<p style="text-align:justify;">FastQC vise à fournir un moyen simple de réaliser des contrôles de qualité sur les données de séquence brutes provenant des pipelines de séquençage à haut débit. Il propose un ensemble modulaire d’analyses qui vous permettent de mettre en évidence d'éventuels problèmes dans vos données,  dont vous devriez être conscient avant de procéder à toute analyse ultérieure.</p>

Sur la [page du projet FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/), vous pouvez trouver la [Documentation](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/) et quelques exemples de rapports.

Quelques articles explicatifs sur des graphiques en échec issus de FastQC,  sont accessibles sur [https://sequencing.qcfail.com/software/fastqc/](https://sequencing.qcfail.com/software/fastqc/).

<p style="text-align:justify;">Dans le cadre du pipeline Nextflow nf-core/RNAseq, FastQC est lancé 2 fois dans la "partie 1 : préparation des données". FastQC est lancé avant et après le nettoyage des séquences non alignées afin de pouvoir comparer la qualité des données avant et après leur nettoyage.</p>

<img src="../../img/Pipeline_RNAseq_FastQC.png">

**Figure1 : pipeline RNAseq simplifié.**