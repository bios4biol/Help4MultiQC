# fastp

[fastp](https://academic.oup.com/bioinformatics/article/34/17/i884/5093234) est un outil de nettoyage des séquences pour en améliorer la qualité. Il coupe les adaptateurs, supprime les séquences trop petites, trop longues ou de trop mauvaise qualité.

Il a été créé par [Shifu Chen](https://scholar.google.com/citations?user=tW47uPIAAAAJ), [Zhou Yanqing](https://www.researchgate.net/profile/Zhou_Yanqing2), [Yaru Chen](https://scholar.google.com/citations?user=nntUUqcAAAAJ) et [Jia Gu](https://jia-gu.github.io/).


fastp est utilisé dans le préprocessing des données, il prends donc en entrée les séquences en format FastQ.

<img src="../../img/Pipeline_SAREK_Fastp.png">

**Figure 1 : pipeline SAREK simplifié.**