## Lectures filtrées

Toutes les lectures passent par le filtre fastp.

Voici un résultats auquel il est possible de s'attendre pour des données de SAREK :

<img src="../../img/fastp_filtered_reads_plot.png"  width="650" height="400"> 

Légende :
<ul>
<li><p style="text-align:justify;"><b>Pass filter</b> indique le nombre de lectures qui ont passé les filtres. Le premier filtre est un filtre de qualité qui ne laisse passer que les lectures avec un prhed quality supérieur ou égale à 15. Le deuxième est centré sur la longueur minimale ou maximale acceptée, il est paramétré sur 0 par défaut, mais peut être modifiable. Le troisième est un filtre de complexité, si une lecture a trop de nucléotide similaire côte à côte, sa complexité sera trop basse. La valeur par défaut est de 30, soit 30 % de complexité est requis pour passer le filtre. </p></li>
<li><b>Low quality</b> indique le nombre de lectures qui n'a pas passé les filtres indiqués précédemment.</li> 
<li><b>Too many N</b> indique le nombre de lectures contenant trop de N. Les N sont des substitutions mises par les séquenceurs en cas de doute trop élevé lors du séquençage du nucléotide.</li>
<li><b>Too short</b> indique le nombre de lectures trop petites.</li> 
<li><b>Too long</b> indique le nombre de lectures trop longues.</li>
<ul>
La figure 1 représente un bon résultat où la grande majorité des lectures passent le filtre. Pour un ADN préservé attendez vous à avoir au moins 80 % de lectures passant le filtre. Il y a quand même certains cas de séquences de trop basses qualités, avec trop de N ou trop petites en taille, ce qui est normal pour du séquençage Illumina. Le fait qu'il n'y ai pas de lectures trop longues est expliqué lui aussi par le séquençage illumina.