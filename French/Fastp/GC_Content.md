## Contenu en GC

<img src="../../img/fastp-seq-content-gc-plot.png"> 

**Figure 1 : pourcentage en GC moyen des lectures pour chaque échantillon avant filtration.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

<p style="text-align:justify;">Dans la figure 1, l'axe des abscisses est composé des positions des lectures et l'axe des ordonnées du taux de GC présent. 
Le taux GC est une métrique classique pour mesurer si la qualité des lectures est bonne. Pour les gènes, on s'attend à un ratio de 50 % constant sur les lectures. Ici, c'est clairement ce qu'on observe donc tout va bien.</p>

<img src="../../img/fastp-seq-content-gc-plot(1).png"> 

**Figure 2 : pourcentage en GC moyen des lectures pour chaque échantillon après filtration.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

Dans la figure 2 après-filtre, nous n'observons pas de différence avec la figure 1 donc la majorité des informations ont été conservées et le séquençage était de très bonne qualité.