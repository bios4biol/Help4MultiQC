## Insert Size

L'insert size est une séquence comprenant l'ensemble constitué des 2 lectures appariées et de l'inner distance.

<img src="../../img/fgene-05-00005-g001.jpg"  width="650" height="400"> 

**Figure 1 : Insert size venant un séquençage Illumina**

*Source : [Filter BAM/SAM files by insert size](https://accio.github.io/bioinformatics/2020/03/10/filter-bam-by-insert-size.html)*

<img src="../../img/fastp-insert-size-plot.png">

**Figure 2 : pourcentage des longueurs d'Insert size pour chaque échantillon**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

<p style="text-align:justify;">Dans la figure 2, on observe les distributions des différents insert size avec une statistique croissante atteignant un pic vers les 150 paires de bases avant de retomber. Sachant que les lectures sont de 120 paires de bases environ, cela donne une innner distance qui est majoritairement autour de -90.</p>