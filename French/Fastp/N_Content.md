## Contenu en N des lectures

<img src="../../img/fastp-seq-content-n-plot.png">

**Figure 1 : taux de N moyen des lectures pour chaque échantillon avant filtre.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*


Dans la figure 1, la position des lectures est indiquée sur l'axe des abscisses et le taux de N moyen des lectures sur l'axe des ordonnées. Les 2 échantillons sont indiqués dans le graphique mais ils se superposent. 
La figure 1 montre des valeurs très basses. C'est ce à quoi l'on s'attend pour des échantillons de grandes qualités.

<img src="../../img/fastp-seq-content-n-plot(1).png"> 

**Figure 2 : taux de N moyen des lectures pour chaque échantillon après filtre.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

On peut clairement voir qu'il n'y a pas de grandes évolutions après filtre, ce qui veut dire que le séquençage s'est très bien passé et que les données sont de très grande qualité.