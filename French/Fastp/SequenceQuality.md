## Qualité des lectures

<img src="../../img/fastp-seq-quality-plot.png"> 

**Figure 1 : qualité moyenne des lectures pour chaque échantillon avant filtre.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

<p style="text-align:justify;">Dans la figure 1, la position des lectures est indiquée sur l'axe des abscisses et la qualité moyenne des lectures sur l'axe des ordonnées. Les 2 échantillons sont indiqués dans le graphique, mais ils se superposent.</p>

<p style="text-align:justify;">On observe 2 choses, que pour les premiers et derniers nucléotides la qualité est plus basse que sur le reste des bases et que la qualité baisse au fur et à mesure que l'on s'approche de l'extrémité 5'. Ceci est une observation classique, il est inutile d'y porter attention. Par contre, des qualités en dessous de 30 peuvent être problématiques et les lectures correspondantes seront sûrement trimmées.</p>

<img src="../../img/fastp-seq-quality-plot(2).png"> 

**Figure 2 : qualité moyenne des lectures pour chaque échantillon après filtration**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

On peut voir qu'il n'y a pas de grandes évolutions après filtre ce qui veut dire que le séquençage s'est très bien passé et que les données sont de très grande qualité.