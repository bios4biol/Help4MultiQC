## Feature Counts

### Quantification des types biologiques

L’outil FeatureCounts réalise une quantification en comparant les lectures alignées au génome de référence. Il compare ensuite ces données avec l'alignement des lectures.

<img src="../../img/featureCounts_assignment_plot.png" width="650" height="400">

**Figure 1 :diagramme en barres des différentes assignations du mapping sur génome de référence, pour chaque échantillon.**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

Légende : 
<ul>
<li><b>Assigned</b> indique le pourcentage de lectures ayant été assignées à une caractéristique biologique précise.</li> 
<li><b>Ambiguity</b> indique le pourcentage de lectures qui a été attribuée à deux caractéristiques biologiques ou plus.</li> 
<li><p style="text-align:justify;"><b>Multimapping</b> indique le pourcentage de lectures qui a été localisée à deux emplacements distincts au sein du génome de référence.</li> 
<li><b>No features</b> suggère que les lectures proviennent soit d’un intron, soit d’une région intergénique.</p></li>
</ul>

<img src="../../img/featurecounts_biotype_plot-1.png" widht="650" height="400"> 

**Figure 2 : diagramme en barres des diverses attributions biologiques des lectures pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">En fonction du génome de référence et du fichier GTF/GFF qui est mis en input, il peut y avoir énormément d’information biologique sur le mapping des lectures. Pour les données RNAseq, ce graphique peut être très intéressant pour savoir quel type d’expression biologique était présent au moment de la capture. En tant que données RNAseq, le graphique ci-dessus est très représentatif des données attendues, étant donné que la plus grande partie des lectures de chaque échantillon sont attribuées à du <b>Protein_coding</b>, ce qui est précisément ce que l’on cherche à mesurer en RNAseq.</p>

### Attention :

<p style="text-align:justify;">Sur le dernier graphique, il y a beaucoup de légendes différentes, cela peut rendre difficile le traitement des données à cause du nombre limité de couleurs. Pour être sûr de l'analyse, il ne faut pas hésiter à cliquer 2 fois sur les légendes afin de les voir apparaître ou disparaître. Bien sûr, placer le curseur sur l’échantillon donnera toutes les infos nécessaires.</p>
