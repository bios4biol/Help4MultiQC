## Feature Counts

[Feature counts](https://subread.sourceforge.net/featureCounts.html)  est un outil de quantification de données génomiques. Il permet de compter les caractéristiques des génomes telles que les gènes et exons.

Il prend en entrée des fichiers SAM/BAM des séquences testées et un fichier d’annotation GTF du génome de référence. Il fait partie du paquet [Subread](https://subread.sourceforge.net/).


<p style="text-align:justify;">Dans le cadre du pipeline Nextflow nf-core/RNAseq, FeatureCounts est lancé dans la "partie 5 : statistiques". Il se sert de l’alignement obtenu sur le génome de référence pour estimer quels sont les biotypes exprimés.</p>

<img src="../../img/Pipeline_RNAseq_FeatureCounts.png">

**Figure1 : pipeline RNAseq simplifié.**