## distribution cumulé du mapping


<img src="../../img/mosdepth-cumcoverage-dist-id.png"> 

**Figure 1 : pourcentage de couverture cumulé pour le nombre de couvertures.**

*Source : [SAREK example MultiQC Nextflow/nf-core](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_aws/multiqc/)*

Dans la figure 1, le pourcentage de couverture est en ordonnée et le nombre de couverture est en abscisse.

