# MothDepeth


<p style="text-align:justify;"> <a href="https://snakemake-wrappers.readthedocs.io/en/0.77.0/wrappers/mosdepth.html"> MothDepeth </a>  est un outil d'analyse de données alignées. Dans le pipeline SAREK, il est utilisé dans l'étape 3 : Statistique.</p>

Il a été mis en ligne par [Brent S Pedersen](https://github.com/brentp) et [Aaron R Quilan](https://bioscience.utah.edu/faculty/quinlan/).

<p style="text-align:justify;"> Si vous rencontrez des difficultés liées à l'utilisation de l'outil Cutadapt, il vous est possible d'ouvrir une issue sur leur <a href="https://github.com/brentp/mosdepth/issues"> gitlab </a></p>


<img src="../../img/Pipeline_SAREK_Mosdepth.png">

**Figure 1 : pipeline SAREK simplifié.**