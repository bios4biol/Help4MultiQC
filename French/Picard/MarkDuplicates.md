## Picard

### Mark Duplicates

MarkDuplicates utilise les informations contenues dans les fichiers BAM/SAM pour présenter les niveaux de duplication dans les échantillons.

<img src="../../img/picard_deduplication.png" title="Mark duplicates" width="650" height="400"> 

**Figure 1 : diagramme en barres des lectures triées en fonction de leurs alignements et duplicatas.**

*source : [nf-core SAREK MultiQC](https://nf-co.re/sarek/3.4.3/results/sarek/results-e92242ead3dff8e24e13adbbd81bfbc0b6862e4c/test_full_germline_ncbench_agilent/multiqc/?file=multiqc_report.html)*

Légende :
<ul>
<li><b>Unique pairs</b> correspond aux lectures appariées à une seule autre lecture.</li>
<li><b>Duplicate Pairs Optical</b> fait référence aux duplicatas venant d'une mauvaise identification de cluster pendant séquençage Illumina.</li>  
<li><p style="text-align:justify;"><b>Duplicate Pairs Nonoptical</b> désigne les duplicatas appariés qui ne sont pas liés au problème optique d’Illumina. Cela peut provenir de la surexpression d’un gène, d’un problème de PCR, de clustering (situation où un cluster occupe deux puits pendant la génération de celui-ci) ou par Sister, où des duplicatas apparaissent suite à la création de brins complémentaires de séquences du cluster original.</p></li>
<li><b>Duplicate Unpaired</b> fait référence aux lectures dupliquées ni séquencées ni mappées.</li>
<li><b>Unmapped** correspond aux lectures non alignées.</li>
<li><b>Unique Unpaired** désigne les lectures sans appariement ni duplicata.</li>
</ul>
<img src="../../img/Duplicates.illumina.png" title="duplicates illumina" width="650" height="400"> 

**Figure 2 : causes principales de duplication en séquençage Illumina.**

*source : [Illumina description](http://core-genomics.blogspot.com/2016/01/almost-everything-you-wanted-to-know.html)* 

<img src="../../img/picard_deduplication-1.png" width="650" height="400"> 

**Figure 3 : diagramme en barres des lectures triées en fonction de leurs alignements et duplicatas pour chaque échantillon..**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">Dans les données RNAseq, il est courant de trouver de tels taux de duplications. Cependant, il est important de noter que ces taux sont conséquents. Cette situation peut être due à une forte expression de certains gènes ou à des problèmes liés au séquençage Illumina.</p>

### Pour des données DNAseq : 

<img src="../../img/picard_deduplication_stats.png" width="750" height="400"> 

**Figure 4 : différents graphiques et leurs interprétations.**

*Source : [nf-core eager](https://nf-co.re/eager/2.5.2/docs/output/)*

Nous attendons beaucoup moins de duplicatas avec des données DNAseq.