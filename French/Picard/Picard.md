## Picard

[Picard](https://broadinstitute.github.io/picard/) est un ensemble d’outils permettant de manipuler efficacement des données de séquençage à haut débit.

Il est possible de signaler des problèmes liés à Picard sur leur [GitHub](https://github.com/broadinstitute/picard/issues).

<p style="text-align:justify;">Dans le cadre du pipeline Nextflow nf-core/RNAseq, Picard est lancé dans la "partie 5 : traitement des données alignées". Il se sert des lectures alignées pour quantifier celles qui sont issues de duplications.</p>

<img src="../../img/Pipeline_RNAseq_Picard.png">

**Figure 1 : pipeline RNAseq simplifié.**