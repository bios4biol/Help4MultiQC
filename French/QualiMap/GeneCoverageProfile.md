## QualiMap

### Profil de la couverture des gènes

L’outil Quali Map utilise les annotations des fichiers GTF du génome de référence pour analyser la couverture des lectures de chaque échantillon.

<img src="../../img/qualimap_gene_coverage_profile-1.png" title="Gene Coverage Profile"> 

**Figure 1 : couverture des gènes par les lectures pour chaque échantillon**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

<p style="text-align:justify;">Dans la première figure, l’ordonnée représente la profondeur cumulative des lectures mappées et l’abscisse représente la position dans le transcrit divisée en 100. Chaque échantillon est représenté par une couleur différente, ce qui permet de comparer les tailles des échantillons, mais pas encore leur recouvrement.</p>

<p style="text-align:justify;">Le graphique montre des valeurs plus faibles aux extrémités 5’ et 3’. Cela est normal, car moins de lectures passent le contrôle de qualité à ces extrémités en raison de leur petite taille ou d’un plus grand nombre d’erreurs lors du séquençage. La différence devrait être la même aux deux extrémités, car les extrémités 5’ et 3’ ne sont pas calculées séparément. Dans l’exemple ci-dessus, il y a une différence dans les derniers pourcentages de couverture des gènes, qui est beaucoup plus faible que les premiers pourcentages.</p>