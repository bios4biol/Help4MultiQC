## QualiMap

## Origine génomique des lectures

<p style="text-align:justify;">QualiMap génère différents graphiques pour aider au contrôle de qualité des données de séquençage d’alignement. QualiMap obtient ces informations à partir des fichiers d’annotation GTF du génome de référence.</p>

<img src="../../img/qualimap_genomic_origin-1.png" title="Gene Origin of reads"> 

**Figure 1 : origines génomiques des lectures pou chaque échantillon.**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

<p style="text-align:justify;">Ce graphique montre l’origine génomique des données. Pour les données RNAseq, nous nous attendons à une proportion élevée de lectures provenant des exons en raison de la nature transcriptomique de l’ARN séquencé. Les parties intragéniques proviennent des introns, et les régions intergéniques comprennent tout le reste.</p>