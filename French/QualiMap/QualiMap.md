## QualiMap

[Qualimap](qualimap.conesalab.org) est un outil d’évaluation de séquençage. Il permet de traiter des données RNAseq, ChIP-seq, de séquençage de génomes entiers et de séquençage d’exomes entiers.

Pour des questions sur l'outil, il est possible de rejoindre le [Qualimap Google group](https://groups.google.com/g/qualimap).

Il a été créé par [Konstantin Okonechnikov](https://www.dkfz.de/en/paediatrische-neuroonkologie/staff/Konstantin_Okonechnikov.html), [Sonia Tarazona](https://www.researchgate.net/profile/Sonia-Tarazona) et [Ana Conesa](https://scholar.google.com/citations?user=KMTiIH4AAAAJ) en lien avec [l'institut de Max Planck pour la biologie infectieuse](https://www.mpiib-berlin.mpg.de/) et le [CIPF](https://bioinfo.cipf.es/cbl/).

Dans le cadre du pipeline Nextflow nf-core/RNAseq, Qualimap est lancé dans la "partie 5 : statistiques". Il va traiter les transcrits obtenus.

<img src="../../img/Pipeline_RNAseq_Qualimap.png">

**Figure 1 : pipeline RNAseq simplifié.**