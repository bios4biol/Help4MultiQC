## RSeQC

### BAM Statistiques

<img src="../../img/BamStat.png" width="650" height="400"> 

**Figure 1: valeurs des différentes métriques pour les échantillons.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans la figure 1, l'axe des abscisses représente la quantité soit en lectures soit en nucléotides (en million).

Bam_Stat.py présente toutes les métriques et les valeurs pour chaque échantillon présent dans les fichiers BAM.

### Métriques:

**Total records :** correspond au nombre total de lectures du fichier BAM.

**QC failed :** <p style="text-align:justify;">correspond aux lectures qui n’ont pas passé le test de qualité. Pour les données RNAseq, le test de qualité a été effectué par FastQC, pour SAREK, il est directement effectué avec l’alignement BWA-mem.</p>

**Duplicates :** <p style="text-align:justify;">les lectures dupliquées sont des lectures ayant les mêmes séquences. Pour les données RNAseq, elles peuvent apparaître pendant la PCR, mais aussi à cause d’une forte expression de gènes. Les valeurs peuvent être très élevées pour des données RNAseq.</p>

**Non primary hit :** <p style="text-align:justify;">correspond à des lectures qui peuvent être soit secondaires (donc qui s’alignent bien à plusieurs endroits du génome de référence) soit supplémentaires (donc qui ne peuvent être représentées par un alignement linéaire).</p>

**Unmapped :** correspond aux lectures non mappées.

**Unique :** correspond aux lectures mappées.

**Read-1 :** première lecture.

**Read-2 :** lecture appariées.

**+ve strand :** il s’agit des lectures appartenant au brin forward.

**-ve strand :** il s’agit des lectures appartenant au brin reverse.

**Non-splice reads :** il s’agit des lectures non épissées possédant ne possédant pas d'introns mais des exons uniquement.

**Splice reads :** il s'agit de lectures ne chevauchant pas d'introns.

**Proper pairs :** il s’agit des lectures qui sont correctement appariées et mappées.

**Different chrom :** correspond aux lectures qui sont appariées avec une lecture présente sur un autre chromosome.
