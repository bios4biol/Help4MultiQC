## RSeQC

### Sens de brins des lectures

<img src="../../img/rseqc_infer_experiment_plot-1.png" width="650" height="400"> 

**Figure 1 : diagramme en barre représentant le sens des brins pour les lectures.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

L'axe des ordonnées représente les échantillons et l'axe des abscisse le pourcentage d'attribution des sens des brins pour les lectures.

<p style="text-align:justify;">infer_experiment.py permet de déterminer sur quel brin les lectures se sont alignées durant l’alignement (sens ou antisens) à partir du brin du gène auquel elles se sont alignées.</p>

<p style="text-align:justify;">Comme on peut le voir dans la figure 1, la majorité des lectures se sont alignées sur des gènes présents sur le brin antisens. La partie <b>undetermined</b> correspond aux cas où le gène peut être lu à plusieurs endroits dans différents sens. Il est donc impossible de déterminer le brin des lectures transcrites de ce gène-là.</p>


<img src="../../img/paired-end-read-1.jpg">

**Figure 2 : schema représentant un fragment et son contenu.**

*source : [Biostar Issues](https://www.biostars.org/p/9593158/)*
