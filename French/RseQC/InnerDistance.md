## RSeQC

### Distance interne entre 2 lectures appariées

<img src="../../img/rseqc_inner_distance_plot-1.png" width="650" height="400"> 

**Figure 1 : distribution des distances internes pour chaque échantillon (Inner distance).**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

L'axe des ordonnées représente le nombre d'occurences et l'axe des abscisses l'inner distance en paires de bases (pb).

<p style="text-align:justify;">L’inner Distance est la distance entre deux lectures appariées. Si les deux lectures se chevauchent, la valeur sera négative. Il est important de supprimer les appariements avec des inners distances trop supérieures à la moyenne, qui donnent moins d'informations. Il est possible que le génome de référence ne soit pas le bon et que les échantillons soient trop différents à celui-ci.</p>

Il permet de vérifier la taille des fragments. Les résultats de la figure montre une distribution attendue. Une distribution de moins bonne qualitée serait beaucoup plus étendues.

<img src="../../img/paired-end-read-1.jpg">

**Figure 2 : schema représentant un fragment et son contenue.**

*source : [Biostar Issues](https://www.biostars.org/p/9593158/)*
