## RSeQC

### Annotation des jonctions

Durant l’événement d’épissage de l’ARNm en ARN, les introns sont épissés pour laisser place aux exons, qui se joignent ensuite. Pendant ces événements, les exons se joignent ensuite. C’est cela que [JunctionAnnotation.py](https://rseqc.sourceforge.net/#junction-annotation-py) va mesurer.

<img src="../../img/rseqc_junction_annotation_junctions_plot-1.png" width="650" height="400"> 

**Figure 1 : diagramme en barres des jonctions trouvées dans les échantillons.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

L'axe des ordonnées représente les échantillons et l'axe des abscisses représente le pourcentage des attributions des types de jonctions.

<p style="text-align:justify;">Les jonctions trouvées grâce aux annotations des fichiers BAM/SAM peuvent soit avoir déjà été référencées dans le génome de référence, soit n’être que partiellement connues, ce qui arrive lorsqu’un des sites donneur ou accepteur n’était pas annotée dans le génome de référence, soit être totalement nouvelles, donc aucun des sites donneur ou accepteur, n’était connue de la jonction.</p>

Il y a aussi les événements d’épissage qui peuvent être annotés et qui ont les mêmes trois catégories que les jonctions.

<p style="text-align:justify;">Pour un génome connu, il est normal d’avoir beaucoup d’événements ou de jonctions d’épissage connus, mais il y a quand même une grande portion de jonctions d’épissage nouvelles ou partiellement nouvelles. Pour l’Homme, on s’attend à des valeurs d’au moins 50% pour les jonctions connues. Pour les autres espèces, tout dépend de la connaissance de l’organisme.</p>