## RSeQC

### Saturation de jonctions

<img src="../../img/rseqc_junction_saturation_plot-1.png" width="650" height="400"> 

**Figure 1 : saturation des jonctions d’épissage pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans cette figure il y a le nombre de jonctions en ordonnée contre le pourcentage de lectures en abscisses.

<p style="text-align:justify;">Le but de JunctionSaturation.py est de trouver la profondeur de séquençage suffisante pour obtenir le nombre maximum de jonctions d’épissage trouvées. Dans la première figure, les courbes continuent d'augmenter, cela veut dire qu'il y a encore des jonctions d'épissage à trouver. C’est en cliquant sur une des courbes que l’on obtient la figure 2.</p>

<img src="../../img/JuctionSaturationMCF7.png" width="650" height="400">

**Figure 2 : saturation de 2 familles de jonctions d'épissage et de la somme totale (en bleu) pour un échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans cette figure, il y a le nombre de jonctions en ordonnée contre le pourcentage de lectures en abscisses.

Dans la figure 2, on observe 3 métriques : La première **All Junctions** est l’addition des 2 autres métriques. **Known Junctions** indique toutes les jonctions trouvées dans le séquençage qui étaient connues du génome de référence. Et **Novel Junctions** indique les jonctions trouvées qui ont soit la partie receveur, soit la partie accepteur soit les 2 extrémités de jointure qui étaient inconnues du génome de référence.

<p style="text-align:justify;">On observe que la courbe des jonctions connues arrive à un plateau, ce qui signifie que la profondeur de séquençage était adaptée, et que l'on est proche de la mesure maximale des jonctions connues. En revanche, la courbe verte des nouvelles jonctions est encore en croissance nette, ce qui veut dire qu’une plus grande profondeur de séquençage permettrait de trouver encore plus de jonctions d’épissage. Il faut noté en revanche que les jonctions nouvelles peuvent être de fausses jonctions.</p>
