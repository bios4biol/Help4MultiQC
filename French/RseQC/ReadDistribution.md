## RSeQC 

### Distribution des lectures

<img src="../../img/rseqc_read_distribution_plot-1.png" width="650" height="400"> 

**Figure 1 : diagramme en barres des caractéristiques génomiques des lectures pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Chaque échantillon indiqué en ordonnée est divisé en pourcentage sur l'axe des abscisses.

Légende:

**CDS_Exons** fait référence à la partie codante de chaque exon (à l’exclusion des 5’ UTR ou 3’ UTR). **5’UTR_Exons** désigne la partie 5’ UTR des exons, et **3’UTR_Exons** désigne la partie 3’ UTR des exons. **Introns** est explicite. **TSS_up_1kb/1kb-5kb/5kb-10kb** correspond aux sites de démarrage de la transcription, divisés en trois parties : jusqu’à 1 kb, 1-5 kb, et 5-10 kb. **TES_down_1kb/1kb-5kb/5kb-10kb** correspond aux sites de fin de transcription, également divisés en trois parties : jusqu’à 1 kb, 1-5 kb, et 5-10 kb. **Other_intergenic** contient le centromère et d’autres régions non définies.

Pour les données RNAseq, vous devriez observer une majorité de CDS et de terminaisons UTR. Cependant, il est normal de trouver des introns venant de gènes très exprimés.