## RSeQC

### Duplications des lectures

[ReadDuplication.py](https://rseqc.sourceforge.net/#read-duplication-py) utilise deux techniques pour mesurer la duplication. Soit elle mesure le nombre de lectures ayant la même séquence, soit elle mesure le nombre de lectures s’alignant au même endroit sur le génome de référence.

Dans le pipeline nf-core, la mesure est basée sur l’alignement, donc sur le génome de référence.

<img src="../../img/rseqc_read_dups_plot-1.png" width="650" height="400"> 

**Figure 1 : duplications des lectures, en fonction de l'alignement, pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

<p style="text-align:justify;">L'axe des abscisses indique le nombre de duplications qu'ont les lectures à un point donné.
L’axe des ordonnées du nombre de lectures est en Log10, ce qui signifie que les valeurs augmentent de manière exponentielle selon cet axe. Il ne faut donc pas se fier à la proximité entre les plus grandes valeurs et les plus petites valeurs. Par exemple, dans le graphique ci-dessus, on observe 60 000 000 lectures avec une occurrence contre 1 000 lectures avec 100 occurrences, la différence est énorme. Si l’on ne s’attend pas à des phénomènes de duplication, la courbe devrait être presque un angle droit avec une valeur énorme à l’occurrence 1 et des valeurs qui chutent brusquement juste après. Dans le graphique représenté, on observe néanmoins beaucoup de duplications, notamment pour les occurrences de 2 à 50. Cela provient du jeu de données RNAseq où l’on s’attend à avoir des duplications dues à la surexpression de certains gènes.</p>
