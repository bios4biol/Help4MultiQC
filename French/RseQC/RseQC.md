## RseQC

[RseQC](https://rseqc.sourceforge.net/) est un outil de contrôle de qualité de données issues de séquençage à haut débit. Il est spécialisé en données RNAseq.

[Ici](https://academic.oup.com/bioinformatics/article/28/16/2184/325191) se trouve le lien vers l’article de présentation de l’outil.

Il a été réalisé par [Liguo Wang](https://scholar.google.com/citations?user=9sQzL0IAAAAJ), Wei Li et Shengqin Wang.

Dans le cadre du pipeline Nextflow nf-core/RNAseq, RSeQC est lancé dans la "partie 5 : statistiques". L'outil fournit différentes statistiques  sur les transcrits et les lectures.

<img src="../../img/Pipeline_RNAseq_RSeQC.png">

**Figure 1 : pipeline RNAseq simplifié.**