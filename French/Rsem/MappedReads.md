## RSEM 

### Alignement des lectures

L’outil RSEM est conçu pour estimer les niveaux d’expression des gènes à partir de données RNAseq.

<img src="../../img/rsem_assignment_plot-1.png" width="650" height="400"> 

**Figure 1 : attribution des lectures après alignement.**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

Dans le graphique ci-dessus, les échantillons sont comparables les uns aux autres et divisés en 4 catégories :

<lu>
<li><b>Aligned uniquely to a gene</b> représente les pourcentages de lectures qui sont alignées sur un seul gène.</li>
<li><b>Aligned to multiple genes</b>  représente les pourcentages de lectures qui sont alignées sur plusieurs gènes.</li>
<li><p style="text-align:justify;"><b>Filtered due to too many alignments</b> est le pourcentage de lectures qui ont été filtrées car elles étaient alignées à trop d’endroits sur les génomes de référence.</p></li>
<li><b>Unalignable reads</b> correspond au lectures qui ne peuvent pas être alignées sur la référence.</li>
</lu>

Si vous avez un pourcentage trop élevé de lectures non alignables, cela pourrait être dû à une contamination ou à une trop grande différence entre le génome de référence et les séquences de vos échantillons.
