## RSEM 

### Taux de multimapping

L’outil RSEM est conçu pour estimer les niveaux d’expression des gènes à partir de données RNAseq.

<img src="../../img/rsem_multimapping_rates-1.png" width="650" height="400"> 

**Figure 1 : cas de multimaping des lectures pour chaque échantillon.**

*source : [MultiQC example RNAseq](https://multiqc.info/example-reports/rna-seq/)*

<p style="text-align:justify;">Dans la figure 1, en ordonnée il y a le nombre de lectures et en abscisse le nombre d'alignements correspondant.
Dans cet exemple, l’écrasante majorité des lectures présentent 1 ou 2 alignements, ce qui constitue un résultat satisfaisant. Toutes les lectures avec 0 alignement, correspondent aux lectures qui n’ont trouvé aucune correspondance avec le génome de référence.</p>

<p style="text-align:justify;">Le premier pic observé ici est typiquement ce à quoi on s’attendrait pour des données RNAseq. Si d’autres pics sont présents, ils pourraient être liés à des problèmes de séquençage. Une autre possibilité est que le génome étudié comporte de nombreuses duplications.</p>
