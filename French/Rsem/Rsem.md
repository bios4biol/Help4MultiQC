## RSEM

[RSEM](https://pubmed.ncbi.nlm.nih.gov/21816040/) est un outil de quantification des données RNAseq. Il peut être utilisé avec ou sans génome de référence.

Il a été réalisé par [Bo Li](https://aisecure.github.io/) et [Colin N Dewey](https://www.biostat.wisc.edu/~cdewey/).

Voici le lien vers les issues du [Gitlab](https://github.com/deweylab/RSEM/issues) en cas de problème. 

Dans le cadre du pipeline Nextflow nf-core/RNAseq, RSEM est lancé dans la "partie 3 : quantification".

<img src="../../img/Pipeline_RNAseq_RSEM.png">

**Figure 1 : pipeline RNAseq simplifié.**