## STAR

### Pourcentage des lectures selon leurs types de mapping.

<img src="../../img/star_alignment_plot-1.png"  width="650" height="400">

**Figure 1 : diagramme en barres représentant les lectures catégorisées par leur famille d'alignement.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.12.0/results/rnaseq/results-3bec2331cac2b5ff88a1dc71a21fab6529b57a0f/aligner_star_salmon/multiqc/star_salmon/?file=multiqc_report.html)*

Dans la figure 1, chaque échantillon est représenté en ordonnée et le pourcentage des lectures en abscisses.

Métriques :

**Uniquely mapped :** lectures qui ont le meilleur score d'alignement.

**Mapped to multiple loci :** lectures qui peuvent être gardées ou filtrées selon l'objectif du projet.

**Mapped to too many loci :** lectures filtrées mais pas alignées car elles sont présentes trop de fois à travers le génome de référence. Si une lecture est mappée plus de 10 fois elle ne sera pas alignée. Ce nombre est paramétrable (voir la documentation STAR.).

**Unmapped too short :** lectures non alignées car le ratio de mapping relatif à l'alignemnet dépassait les 30% de mismatch.

**Unmapped other :** lectures non alignées car 66% de la lecture n'a pas pue être alignée. Un autre cas est possible où la seed d'alignement n'a pas pu être trouvée c'est souvent le cas dans les contaminations par exemple. 

S’il y a trop de séquences multi-mappées, cela correspond à une forte duplication du génome ou à des erreurs de manipulations/séquençages comme metttre plusieurs fois les même lectures par exemple. Le graphique ci-dessus montre un bon résultat : on attend au moins 90% de lectures **Uniquely mapped**.