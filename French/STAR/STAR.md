## STAR

[STAR](https://academic.oup.com/bioinformatics/article/29/1/15/272537?login=true) est un aligneur de données RNAseq épissées réalisé par [Alex Dobin](https://www.cshl.edu/research/faculty-staff/alexander-dobin/)

Ici les [issues](https://github.com/alexdobin/STAR/issues?page=1&q=is%3Aissue+is%3Aopen) du Git.

Dans le cadre du pipeline Nextflow nf-core/RNAseq, STAR est lancé dans la "partie 2 : maapping". C'est un aligneur de lectures sur transcriptome de référence.

<img src="../../img/Pipeline_RNAseq_STAR.png">

**Figure 1 : pipeline RNAseq simplifié.**