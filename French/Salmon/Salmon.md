## Salmon

[Salmon](https://combine-lab.github.io/salmon/) est un outil de quantification transcriptomique pour les données RNAseq.

Il a été développé par [Rob Patro](http://www.robpatro.com/redesign/) à [Stony Brook University](https://www.cs.stonybrook.edu/) en collaboration avec Geet Duggal et Carl Kingsford de biologie computationnelle à Carnegie Mellon University, ainsi que Mike Love à UNC Chapel Hill et Rafael Irizarry à Harvard et au Dana Farber Cancer Institute.

Voici [l'article de présentation de l'outil](https://www.nature.com/articles/nmeth.4197), et la [documentation](https://salmon.readthedocs.io/en/latest/salmon.html).

Dans le cadre du pipeline Nextflow nf-core/RNAseq, Salmon est lancé dans plusieurs parties. D'abord dans la "partie 2 : mapping" en tant qu'aligneur, et puis dans les parties "1 : préparation des données" et "3 : quantification" comme quantifieur des lectures et des transcrits.

<img src="../../img/Pipeline_RNAseq_Salmon.png">

**Figure 1 : pipeline RNAseq simplifié.**