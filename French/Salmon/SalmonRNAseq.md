## Salmon

### Quantification des lectures appareillées

<img src="../../img/salmon_plot-1.png" width="650" height="400"> 

**Figure 1: distribution des longueurs des lectures raboutées pour chaque échantillon.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans la figure 1 représente les longueurs des fragments de lectures appariées sur l'axe des abscisses. En ordonnée, les quantités normalisées pour chaque échantillon. Ces résultats sont en lien avec le graphique [inner distance](https://bios4biol.pages.mia.inra.fr/Help4MultiQC/French/RseQC/InnerDistance.html). La représentation de la taille totale des deux lectures plus l’inner distance pour chaque association nous permet de vérifier si notre librairie de séquençage est bien celle indiquée.

<p style="text-align:justify;">Par exemple, on sait que la taille des lectures ici est de 100 pb pour deux échantillons (GM_12878 et K562) et de 75 pb pour les deux autres (H1 et MCF7). Pour GM_12878_REP2, avec une valeur de 200, son inner distance est d’environ 0. Les deux lectures se recoupent occasionnellement. En revanche, pour H1, on a un pic à 287, donc les lectures appariées ne se recoupent presque pas ou en tout cas beaucoup moins que pour GM_12878.</p>


<img src="../../img/paired-end-read-1.jpg">

**Figure 2 : schema représentant un fragment et son contenu.**

*source : [Biostar Issues](https://www.biostars.org/p/9593158/)*