## Samtools

### Statistiques Samtools

<img src="../../img/Alignemenetmetric.png" title="Alignement metrics"  width="650" height="400">

**Figure 1 : valeurs des métriques attribuées aux différents échantillons.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans la figure, l'axe des abscisses représente la quantité soit en lectures soit en nucléotides (en million).

Pour appréhender ces types de métriques, il est essentiel de comprendre le concept d’un flag. Les flags sont des annotations présentes dans les fichiers SAM ou BAM, qui signalent l’état d’alignement des lectures. Pour une compréhension plus approfondie, nous vous recommandons d’utiliser un site web dédié où vous pourrez vous familiariser davantage avec les différents flags :
https://broadinstitute.github.io/picard/explain-flags.html 

### Métriques:
Dans le cadre du pipeline Nextflow nf-core/RNAseq, DupRadar est lancé dans la "partie 5 : statistiques"
**Total sequences** : il s’agit du nombre total de lectures pour chaque échantillon.

**Mapped & paired** : ce sont les lectures qui sont correctement appariées et mappées. Ces lectures ont le flag 0x1 (lecture appariée),1200 mais pas les flags 0x4 (lecture non mappée) et 0x8 (lecture appariée non mappée).

**Mapped in proper pair** : il s’agit d’une lecture avec les flags 0x2 (lecture mappée) et 0x1 (lecture appariée), qui est correctement mappée et appariée

**Duplicated** : les lectures dupliquées sont des lectures ayant les mêmes séquences. Pour les données RNAseq, elles peuvent apparaître pendant la PCR, mais aussi à cause d’une surexpression d’un gène. Les valeurs peuvent être très élevées pour des données RNAseq. Les duplicatas ont le flag 0x400 (lecture de PCR ou duplicata optique).

**QC Failed** : il s’agit des lectures qui n’ont pas passé le test de qualité. Elles ont le flag 0x200 (lectures échouant un test de qualité). Pour des données RNAseq, le test de qualité a été effectué par FastQC, pour SAREK, il est directement effectué avec l’alignement BWA-mem.

**Reads MQ0** : ce sont les lectures ayant une qualité d’alignement de 0. Il y a trois options pour l’origine de cette qualité : soit la lecture ne fait pas partie de la référence (à cause de contamination ou de génome de référence peu connu), soit le programme de séquençage a manqué la vraie position (à cause de la technique heuristique du programme qui va prendre la première position la plus valable sans tout tester), soit ce n’est pas une bonne correspondance (l’erreur est due au taux de séquence répété élevé dans le génome de référence et l'organisme testé).

**Mapped bases (CIGAR)** : il s’agit du nombre de bases filtrées par les chaînes de caractères CIGAR. CIGAR indique le nombre de nucléotides et ensuite le type de mappage dans son fichier. Le nombre de bases est compté pour : les correspondances (M), les insertions (I), les correspondances de séquences (=) et les mauvaises correspondances de séquences (X), mais il n’est pas compté pour les délétions (D), les régions passées (N), les soft clippings (S) et les hard clippings (H).

**Bases Trimmed** :  il s’agit du nombre de bases trimmées pendant l’alignement. Pour SAREK, il n’y a pas de bases trimmées dans le pipeline, BWA peut faire du soft ou hard clipping. Pour des données RNAseq, l’aligneur STAR fait du soft clipping et HISAT2 trimme pour les extrémités 3’, 5’. Donc, il ne devrait pas y avoir de bases trimmées si les aligneurs STAR ou BWA-mem sont utilisés.


**Duplicated bases** : il s’agit du nombre total de bases des lectures dupliquées.

**Different Chromosomes** : il s’agit du nombre de lectures qui ont leurs lectures appariées sur un chromosome différent.

**Other orientation** : ce sont les paires de lectures qui ne sont ni inward ni outward (voir ci-dessous).

**Inward pairs** : les paires inward sont des lectures caractérisées par le fait que leurs orientations 3’ sont dirigées vers leurs paires sur l’autre brin.

<img src="../../img/image.png" title="Inward pairs">. 


**Figure 2 : inward pairs**

*source : [Illumina Paired End Libraries - Inward and Outwardly Directed Reads](https://homolog.us/blogs/tech/2012/02/19/illumina-paired-end-libraries-inward-and-outward-looking-reads/)* 


**Outward pairs** : Les paires outward sont des lectures qui se caractérisent par le fait que leurs orientations 3’ sont dirigées dans la direction opposée à celle de leurs paires sur l’autre brin.

<img src="../../img/image-1.png" title="Outward pairs">


**Figure 3 : inward pairs**

*source : [Illumina Paired End Libraries - Inward and Outwardly Directed Reads](https://homolog.us/blogs/tech/2012/02/19/illumina-paired-end-libraries-inward-and-outward-looking-reads/)* 
