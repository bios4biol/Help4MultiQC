## Samtools

### Lectures cartographiées par contigs

<img src="../../img/samtools-idxstats-mapped-reads-plot-1.png" width="650" height="400"> 

**Figure 1 : proportion de chaque échantillon en comptage normalisé à travers le génome.**

*source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans la figure 1, les chromosomes sont indiqués sur l'axe des abscisse et la quantité normalisée en axe des ordonnées.

Samtools idxstats fournit des informations sur l’alignement des lectures/contigs des échantillons après leur alignement sur les différentes parties du génome de référence.

Pour le génome humain, les chromosomes numérotés deviennent de plus en plus petits, il est donc normal de voir une diminution progressive le long de l’axe des abscisses. Le chromosome Y est noté à 0 ici, mais il est bien identifié, simplement à un ratio tellement bas par rapport aux autres (ratio <à 0,01%) qu’il n’est pas représenté.

Pour les données RNAseq, la partie “MT” correspond au génome mitochondrial et peut montrer des ratios très élevés en raison de l’expression conséquente de ces organelles.
