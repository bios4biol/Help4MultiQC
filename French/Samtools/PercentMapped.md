## Samtools

## Pourcentage de lectures selon leur qualité de mapping

<img src="../../img/samtools_alignment_plot-1.png" title="Poucentage mappé" width="650" height="400"> 

**Figure 1 : diagramme en barre représentant la qualité de l'alignement pour chaque échantillon.**

*Source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*
 
L'axe des ordonnées représente les différents échantillons et l'axe des abscisses représente les pourcentages des lectures selon leur qualité de mapping.

Il existe trois métriques principales principales :
- **mapped (with Mapping quality(MQ)>0)** : cette métrique correspond au nombre de lectures mappées avec une qualité d’alignement satisfaisante.
- **MQ0** : cette métrique représente le nombre de lectures qui auraient pu être mappées, mais dont la qualité d’alignement est trop basse (MQ=0). 
- **Unmapped** : cette métrique correspond au nombre de lectures non mappées.

Une qualité d’alignement faible indique une probabilité de confiance insuffisante pour cette lecture. Les lectures avec une qualité d’alignement de 0 peuvent signifier deux choses différentes . Soit la lecture ne fait pas partie de la référence, ce qui peut être dû à une contamination ou à un génome non complet. Soit, si la qualité d'alignement est vraiment très basse, cela peut aussi être à cause d'un trop fort niveau de multimapping.

En conséquence, la majorité de vos lectures devraient être mappées à environ 90% pour un génome bien connu. Dans le cas d’un génome de référence moins connu ou lors de l’utilisation d’espèces plus éloignées, ce taux peut diminuer jusqu’à 70/80%.