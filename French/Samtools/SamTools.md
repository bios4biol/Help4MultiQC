## Samtools

[SAMtools](https://www.htslib.org/doc/samtools.html)  est un outil multitâche qui peut notamment permettre l'analyse des fichiers SAM/BAM.

Vous trouverez [ici](https://pubmed.ncbi.nlm.nih.gov/33590861/) les dernières mises à jour des Samtools.

Il ont été développés par [Heng Li](http://liheng.org/)

Dans le cadre du pipeline Nextflow nf-core/RNAseq, Sam est lancé dans la "partie 4 : traitement post-mapping". Il analyse les fichiers BAM/SAM.

<img src="../../img/Pipeline_RNAseq_SamTools.png">

**Figure 1 : pipeline RNAseq simplifié.**