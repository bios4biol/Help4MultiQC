## Samtools

## Samtools flagstats

<img src="../../img/Samtools.Flagstat.png"  width="650" height="400"> 

**Figure 1 : différentes métriques attribuées aux différents échantillons.**

*Source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Dans la figure 1, l'axe des abscisses représente la quantité soit en lectures soit en nucléotides (en million).

Pour appréhender ces types de métriques, il est essentiel de comprendre le concept d’un flag. Les flags sont des annotations présentes dans les fichiers SAM ou BAM, qui signalent l’état d’alignement des lectures. Pour une compréhension plus approfondie, je vous recommande d’utiliser un site web dédié où vous pourrez vous familiariser davantage avec les différents flags :
https://broadinstitute.github.io/picard/explain-flags.html 

### Métriques:
**Total reads** : il s’agit du nombre total de lectures pour chaque échantillon.

**Total Passed QC** : c’est le nombre de lectures qui ont réussi le contrôle de qualité (QC).

**Mapped** : il s’agit de la somme des lectures mappées pour chaque échantillon.

**Secondary Alignments** : les alignements secondaires correspondent aux lectures alignées à d’autres endroits dans le génome de référence. L’alignement principal est celui avec le score d’alignement le plus élevé, et les autres sont considérés comme des alignements secondaires avec le drapeau 0x100 (Pas l'alignemnet principale).

**Duplicates** : les duplicatas sont des lectures ayant les mêmes séquences. Pour les données RNAseq, elles peuvent apparaître lors de la PCR, mais aussi en raison de la surexpression d’un gène. Les valeurs peuvent être très élevées pour les données RNAseq. Les duplicatas ont le drapeau 0x400 (lecture de PCR ou duplicata optique).

**Paired in Sequencing** : il s’agit du nombre de lectures appariées, c’est-à-dire possédant le drapeau 0x1.

**Properly Paired** : ce sont les lectures qui sont correctement appariées et mappées. Ces lectures ont les drapeaux 0x1 (lecture appariée) et 0x2 (lecture mappée), mais pas le drapeau 0x4 (lecture non mappée).

**Self and mate mapped** : il s’agit des lectures qui sont mappées et appariées. Cette métrique est moins stricte que “Correctement appariées”. Les lectures ont le drapeau 0x1 (lecture appariée), mais pas les drapeaux 0x4 (lecture non mappée) et 0x8 (lecture appariée non mappée).

**Singletons** : les lectures singletons sont mappées et appariées avec des lectures non mappées. Elles ont les drapeaux 0x1 (lecture appariée) et 0x8 (lecture appariée non mappée), mais pas le drapeau 0x4 (lecture non mappée).

**Mate mapped to diff chr** : il s’agit du nombre de lectures qui sont mappées et appariées, mais dont les deux lectures se trouvent sur deux chromosomes différents. Les lectures ont le drapeau 0x1 (lecture appariée), mais pas les drapeaux 0x4 (lecture non mappée) et 0x8 (lecture appariée non mappée).

**Diff chr (mapQ>=5)** : il s’agit du nombre de lectures qui sont mappées et appariées, mais dont les deux lectures se trouvent sur deux chromosomes différents tout en ayant une bonne qualité d’alignement. Les lectures ont le drapeau 0x1 (lecture appariée), mais pas les drapeaux 0x4 (lecture non mappée) et 0x8 (lecture appariée non mappée). Leurs qualités d’alignement (mapQ) sont supérieures ou égales à 5.

### Analyse rapide :

<img src="../../img/samtools_flagstat.png"  width="650" height="400"> 

**Figure 2 : indice de bon ou mauvais résultat du graphique.**

*Source : [nf-core eager](https://nf-co.re/eager/2.5.2/docs/output/)*