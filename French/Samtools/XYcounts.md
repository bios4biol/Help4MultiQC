## Samtools

## Comptage des portions des lectures retrouvé dans les chromosomes X et Y

<img src="../../img/samtools-idxstats-xy-plot-1.png" width="650" height="400"> 

**Figure 1 : diagramme en barres représentant la portion de lectures retrouvées dans les chromosomes X et Y pour chaque échantillon.**

*Source : [nf-core RNAseq MultiQC](https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html)*

Le graphique ci-dessus représente les proportions des lectures dans les chromosomes X et Y étalées sur l'axe des abscisses. Cette représentation est utile pour vérifier que notre échantillon provient bien de l’individu séquencé.

Dans ce graphique, on observe constamment la présence du chromosome Y. Cependant, il est important de rester vigilant quant à la représentation de Y, car une partie du chromosome Y est en commun avec le chromosome X. Par conséquent, nous aurons toujours une représentation attribuée à Y. Cependant, c’est la différence entre les proportions attribuées qui nous permet de différencier les hommes des femmes.