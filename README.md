# Help4MultiQC

https://bios4biol.pages.mia.inra.fr/Help4MultiQC/

This page will present how to interpret results metrics compiled by multiQCreport obtained on standard NGS pipelines such as RNAseq or 16S metaGenomics.

This work is coordinated by the CATI Bios4Biol of INRAE.

You can find informations on [MultiQC page](https://multiqc.info/) and especially an example of a [MultiQC report for RNAseq data](https://multiqc.info/examples/rna-seq/multiqc_report.html).


## Getting started

## Support
For any comment, suggestion or issue, please feel free to contact us directly or open an issue (https://forgemia.inra.fr/bios4biol/Help4MultiQC/-/issues).

## Contributing
Anyone who would like to contribute to this work to improve the content or add descriptions for a new QC module is welcome.
We invite them to contact contributing authors.

## Authors and acknowledgment 
This work have been done by CATI Bios4Biol with the contribution of [Gaston Rognon](https://forgemia.inra.fr/gaston.rognon1) @gaston.rognon1,  [Claire Hoede](https://forgemia.inra.fr/claire.hoede) @claire.hoede, [Yannick Lippi](https://forgemia.inra.fr/ylippi) @ylippi , [Cervin Guyomar](https://forgemia.inra.fr/cervin.guyomar) @cervin.guyomar and [Sarah Maman-Haddad](https://forgemia.inra.fr/sarah.maman-haddad) @sarah.maman-haddad.

The content of this Gitbook was mainly inspired by previously published content, the sources of which are listed below. Other sources are listed throughout the GitBook:
* https://elearning.formation-permanente.inrae.fr/course/view.php?id=196&section=1
* https://github.com/nf-core/rnaseq/blob/master/docs/output.md#quality-control
* https://github.com/hbctraining/Intro-to-rnaseq-hpc-salmon/blob/master/lessons/qc_fastqc_assessment.md
* https://youtu.be/qPbIlO_KWN0
* https://multiqc.info/docs/#using-multiqc
* https://rtsf.natsci.msu.edu/genomics/technical-documents/fastqc-tutorial-and-faq.aspx
* https://subread.sourceforge.net/featureCounts.html 
* https://multiqc.info/ 
* https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1276-2 
* https://gatk.broadinstitute.org/hc/en-us/articles/360037052812-MarkDuplicates-Picard
* https://academic.oup.com/bioinformatics/article/28/20/2678/206551?login=true
* https://nf-co.re/rnaseq/3.14.0/results/rnaseq/results-b89fac32650aacc86fcda9ee77e00612a1d77066/aligner_star_rsem/multiqc/star_rsem/?file=multiqc_report.html  
* http://qualimap.conesalab.org/
* https://rnnh.github.io/bioinfo-notebook/docs/featureCounts.html
* https://homolog.us/blogs/tech/2012/02/19/illumina-paired-end-libraries-inward-and-outward-looking-reads/
* https://genome.cshlp.org/content/suppl/2008/09/26/gr.078212.108.DC1/maq-supp.pdf 
* http://www.htslib.org/doc/samtools-flagstat.html
* https://www.biostars.org/p/268550/
* https://support.bioconductor.org/p/91818/
* https://help.galaxyproject.org/t/unassigned-ambiguity-problem-in-featurecounts/5921/2
* https://cutadapt.readthedocs.io/en/stable/
* http://qualimap.conesalab.org/doc_html/analysis.html
* https://kokonech.github.io/qualimap/kidney_rnaseqqc/qualimapReport.html
* https://www.biostars.org/p/14283/
* https://www.seqanswers.com/forum/bioinformatics/bioinformatics-aa/20193-duplication-level-of-rna-seq-data 
* https://rseqc.sourceforge.net/#read-duplication-py
* https://training.galaxyproject.org/training-material/topics/transcriptomics/tutorials/ref-based/tutorial.html
* https://biocontainers.pro/tools/fastp


## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Features MultiQC

### Chart

You can hover over the different samples to get the percentages and raw counts of each sample by contig.

For a better visualization, you can remove categories by clicking on their legends.

You can also zoom in by left-clicking on a part of the graph to the other part you want to zoom in. Simple click on **Reset Zoom** to restore the graph to its initial state.

### Plot

You can hover over the different samples and it will show the quantities on the x-axis.

You can also zoom in by left-clicking on a part of the graph to the other part you want to zoom in. Simple click on **Reset Zoom** to restore the graph to its initial state.

### Violin plot

You can position the cursor on specific points of a sample to visualize their location in other metrics. This action also displays the values of the metric corresponding to the selected point.